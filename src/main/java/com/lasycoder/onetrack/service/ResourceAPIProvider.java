package com.lasycoder.onetrack.service;

import com.lasycoder.onetrack.dto.ResourceUserDetails;

public interface ResourceAPIProvider {
	public abstract ResourceUserDetails fetchUserDetails(String accessToken, String userId); 
}

/**
 * 
 */
package com.lasycoder.onetrack.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.lasycoder.onetrack.dao.InitiativeDAO;
import com.lasycoder.onetrack.dao.ProjectDAO;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.dto.InitiativeListResponse;
import com.lasycoder.onetrack.dto.InitiativeRequest;
import com.lasycoder.onetrack.dto.InitiativeResponse;
import com.lasycoder.onetrack.dto.PaginatedList;
import com.lasycoder.onetrack.model.Initiative;
import com.lasycoder.onetrack.model.Project;
import com.lasycoder.onetrack.util.ApplicationUtil;

/**
 * @author rbarahpu
 *
 */

@Service
public class InitiativeService {

	@Autowired
	private InitiativeDAO initiativeDAO;

	@Autowired
	private ProjectDAO projectDAO;
	
	public Logger logger = LoggerFactory.getLogger(InitiativeService.class);

	public InitiativeResponse addInitiative(InitiativeRequest request) {

		String projectId = request.getProjectId();
		logger.info("Request PROJECT ID - " + projectId);

		InitiativeResponse initiativeResponse = new InitiativeResponse();
		Optional<Project> project = projectDAO.findByProjectId(projectId);
		
		String initiativeId = request.getInitiativeId();
		Optional<Initiative> optInitiative = initiativeDAO.findByInitiativeId(initiativeId);
		
		if (optInitiative.isPresent()) {
			ErrorModel errorModel = new ErrorModel();
			errorModel.setErrorCode(HttpStatus.BAD_REQUEST.toString());
			errorModel.setErrorMessage("An initiative already exist with this Id - " + request.getInitiativeId());
			initiativeResponse.setErrorModel(errorModel);
			initiativeResponse.setResponseCode(HttpStatus.BAD_REQUEST);
			return initiativeResponse;
		}
		if (project.isPresent()) {

			Initiative initiative = new Initiative(request.getInitiativeId(), request.getInitiativeDesc(),
					project.get(), "NEW", System.currentTimeMillis(), ApplicationUtil.getCurrentUserID());

			Initiative i = initiativeDAO.save(initiative);
			initiativeResponse.setInitiativeId(i.getId());
			initiativeResponse.setResponseCode(HttpStatus.CREATED);
			return initiativeResponse;
		}
		logger.info("PROJECT NOT FOUND");
		ErrorModel errorModel = new ErrorModel();
		errorModel.setErrorMessage("Project does not exist - " + projectId);
		initiativeResponse.setErrorModel(errorModel);
		initiativeResponse.setResponseCode(HttpStatus.BAD_REQUEST);
		return initiativeResponse;

	}

	public Optional<Initiative> getInitiativeById(Long id) {

		return initiativeDAO.findById(id);
	}

	public InitiativeListResponse getAllInitiatives(int limit, int offset) {

		Pageable pageable = PageRequest.of(offset, limit);
		Page<Initiative> initiativesPage = initiativeDAO.findAll(pageable);
		long totalRecords = initiativesPage.getTotalElements();
		List<Initiative> initiativeList = initiativesPage.toList();
		List<InitiativeResponse> initiatives = new ArrayList<InitiativeResponse>();
		initiativeList.forEach(i -> {
			InitiativeResponse ir = new InitiativeResponse(i.getId(), i.getInitiativeId(), i.getInitiativeDesc(),
					i.getProject().getProjectId(), i.getStatus(), i.getCreatedTimestamp());
			initiatives.add(ir);

		});
		PaginatedList<InitiativeResponse> paginatedList = new PaginatedList<InitiativeResponse>();
		paginatedList.setResults(initiatives);
		paginatedList.setLimit(limit);
		paginatedList.setOffset(offset);
		paginatedList.setTotalRecords(totalRecords);
		InitiativeListResponse response = new InitiativeListResponse();
		response.setInitiatives(paginatedList);
		response.setResponseCode(HttpStatus.OK);
		return response;

	}
	
	public InitiativeListResponse getInitiativesByProjectId(String projectId, int limit, int offset) {
		
		Optional<Project> project = projectDAO.findByProjectId(projectId);
		InitiativeListResponse response = new InitiativeListResponse();
		
		if(!project.isPresent()) {
			ErrorModel errorModel = new ErrorModel();
			errorModel.setErrorMessage("Project does not Exist - " + projectId);
			response.setErrorModel(errorModel);
			response.setResponseCode(HttpStatus.BAD_REQUEST);
			return response;
		}
		
		Pageable pageable = PageRequest.of(offset, limit);
		logger.info("Finding project by projectId - " + projectId);
		Page<Initiative> initiativesPage = initiativeDAO.findByProjectId(projectId, pageable);
		long totalRecords = initiativesPage.getTotalElements();
		List<Initiative> initiativeList = initiativesPage.toList();
		List<InitiativeResponse> initiatives = new ArrayList<InitiativeResponse>();
		initiativeList.forEach(i -> {
			InitiativeResponse ir = new InitiativeResponse(i.getId(), i.getInitiativeId(), i.getInitiativeDesc(),
					i.getProject().getProjectId(), i.getStatus(), i.getCreatedTimestamp());
			initiatives.add(ir);

		});
		PaginatedList<InitiativeResponse> paginatedList = new PaginatedList<InitiativeResponse>();
		paginatedList.setResults(initiatives);
		paginatedList.setLimit(limit);
		paginatedList.setOffset(offset);
		paginatedList.setTotalRecords(totalRecords);
		
		response.setInitiatives(paginatedList);
		response.setResponseCode(HttpStatus.OK);
		return response;
	}

	public Optional<Initiative> getInitiativeByInitiativeId(String initiativeId) {
		return initiativeDAO.findByInitiativeId(initiativeId);
	}

	public InitiativeResponse deleteInitiativeById(Long id) {
		
		Optional<Initiative> initiative = initiativeDAO.findById(id);
		
		InitiativeResponse initiativeResponse = new InitiativeResponse();
		
		if (!initiative.isPresent()) {
			ErrorModel errorModel = new ErrorModel();
			errorModel.setErrorMessage("Initiative with Id - " + id + " not found");
			initiativeResponse.setErrorModel(errorModel);
			initiativeResponse.setResponseCode(HttpStatus.NOT_FOUND);
			return initiativeResponse;
		}
		initiativeDAO.deleteById(id);
		
		initiativeResponse.setResponseCode(HttpStatus.OK);
		return initiativeResponse;
	}
	
	public InitiativeResponse updateInitiative(InitiativeRequest initiativeReq) {
		InitiativeResponse initiativeResponse = new InitiativeResponse();
		Optional<Initiative> init = initiativeDAO.findByInitiativeId(initiativeReq.getInitiativeId());
		if (init.isPresent()) {
			Initiative initiative = init.get();
			initiative.setInitiativeDesc(initiativeReq.getInitiativeDesc());
			initiative.setStatus(initiativeReq.getStatus());
		
			initiativeDAO.save(initiative);
			
			initiativeResponse.setResponseCode(HttpStatus.OK);
			initiativeResponse.setSuccessMsg("Initiative Updated Successfully");
		} else {
			ErrorModel errorModel = new ErrorModel(HttpStatus.BAD_REQUEST.name(),"Initiative not found");
			initiativeResponse.setResponseCode(HttpStatus.BAD_REQUEST);
			initiativeResponse.setErrorModel(errorModel);
		} 
		return initiativeResponse;
	}

}

/**
 * 
 */
package com.lasycoder.onetrack.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.lasycoder.onetrack.dao.EmployeeDAO;
import com.lasycoder.onetrack.dao.ProjectDAO;
import com.lasycoder.onetrack.dto.EmployeeListResponse;
import com.lasycoder.onetrack.dto.EmployeeRequest;
import com.lasycoder.onetrack.dto.EmployeeResponse;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.dto.PaginatedList;
import com.lasycoder.onetrack.exceptions.ClientException;
import com.lasycoder.onetrack.model.Employee;
import com.lasycoder.onetrack.model.EmployeeRole;
import com.lasycoder.onetrack.model.Project;



/**
 * @author rbarahpu
 *
 */

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private ProjectDAO projectDAO;
	
	public long addEmployee(EmployeeRequest employee,String principalUser) throws Exception {
		Optional<Employee> user = employeeDAO.findByEmailAddress(employee.getEmailAddress());
		if (user.isPresent()) {
			throw new ClientException("Employee's email address already registered");
		} 
		
		Optional<Employee> managerEmp = employeeDAO.findById(employee.getManagerId());
		if (!managerEmp.isPresent() || (managerEmp.isPresent() && !managerEmp.get().getEmployeeRole().equals(EmployeeRole.MANAGER))) {
			throw new ClientException("Manager does not exists or assigned managerId does not exists");
		}
		
		Employee emp = new Employee();
		emp.setFullName(employee.getEmployeeFullName());
		emp.setEmployeeRole(EmployeeRole.valueOf(employee.getEmployeeRole()));
		emp.setManagerId(employee.getManagerId());
		emp.setCreatedTimestamp(System.currentTimeMillis()/1000);
		emp.setUserId(employee.getEmailAddress());
		emp.setEmailAddress(employee.getEmailAddress());
		emp.setCreatedBy(principalUser);
		
		Optional<Project> project = projectDAO.findByProjectId(employee.getProjectId()); 
		emp.setProject(project.get());
		
		emp = employeeDAO.save(emp);
		
		return emp.getId(); 
	}
	
	public void deleteEmployeeById(Long id) {
		employeeDAO.deleteById(id);
	}
	
	public void deleteEmployeeByUserId(String userId) {
		employeeDAO.deleteByUserId(userId);
	}
	
	public Optional<Employee> getEmployeeById(Long id) {
		return employeeDAO.findById(id);
	}
	
	public Optional<Employee> getEmployeeByUserId(String userId) {
		return employeeDAO.findByUserId(userId);
	}
	
	private EmployeeListResponse buildPaginatedEmployeeListResponse(Page<Employee> listOfEmployees,int limit,int offset) {
		List<EmployeeResponse> resultList = new ArrayList<>();
		listOfEmployees.get().forEach(emp -> {
			EmployeeResponse em = new EmployeeResponse();
			
			em.setId(String.valueOf(emp.getId()));
			em.setEmployeeId(emp.getUserId());
			em.setFullName(emp.getFullName());
			em.setManagerId(emp.getManagerId());
			em.setEmployeeRole(emp.getEmployeeRole().toString());
			em.setProjectId(emp.getProject().getProjectId());
			 
			resultList.add(em); 
		}); 
		
		
		PaginatedList<EmployeeResponse> employeeList = new PaginatedList<EmployeeResponse>();
		employeeList.setResults(resultList);   
		employeeList.setTotalRecords(listOfEmployees.getTotalElements());
		EmployeeListResponse employeeListResponse = new EmployeeListResponse();
		
		if (employeeList.getResults().size()==0) {
			ErrorModel error = new ErrorModel();
			error.setErrorCode(String.valueOf(HttpStatus.NOT_FOUND.value()));
			error.setErrorMessage("Results not found"); 
			employeeListResponse.setError(error); 
			employeeListResponse.setResponseCode(HttpStatus.NOT_FOUND);
		} else {
			employeeListResponse.setEmployees(employeeList);   
			employeeListResponse.getEmployees().setLimit(limit);
			employeeListResponse.getEmployees().setOffset(offset); 
			employeeListResponse.setResponseCode(HttpStatus.OK);
		}
		
		
		return employeeListResponse;
	}
	
	public EmployeeListResponse fetchEmployeesBasedOnDynamicQuery(String filterType, Map<String,Object> queryMap,int limit, int offset) throws Exception { 
		Pageable page = Pageable.ofSize(limit).withPage(offset);
		// Page<Employee> listOfEmployees = employeeDAO.findAllEmployeesByProjectWithPagination(queryMap.get("projectId"), page);
		EmployeeSearchQueryBuilder<Employee> query = new EmployeeSearchQueryBuilder<Employee>(filterType,queryMap); 
		Page<Employee> listOfEmployees = employeeDAO.findAll(query,page);
		
		EmployeeListResponse employeeListResponse = buildPaginatedEmployeeListResponse(listOfEmployees,limit,offset);  
		return employeeListResponse;
	}
}

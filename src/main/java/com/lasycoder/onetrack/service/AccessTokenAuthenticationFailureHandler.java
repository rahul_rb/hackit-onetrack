package com.lasycoder.onetrack.service;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccessTokenAuthenticationFailureHandler implements AuthenticationFailureHandler  {
	 
	    private String createErrorBody(AuthenticationException exception) throws JsonProcessingException {
	        Map<String,Object> exceptionMessage = new HashMap<>();
	        exceptionMessage.put("code", HttpStatus.UNAUTHORIZED.value());
	        exceptionMessage.put("reason", HttpStatus.UNAUTHORIZED.getReasonPhrase());
	        exceptionMessage.put("timestamp", Instant.now().toString());
	        exceptionMessage.put("message", exception.getMessage());
	        return new ObjectMapper().writeValueAsString(exceptionMessage);
	    }

		@Override
		public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
				 AuthenticationException exception)
				throws IOException, ServletException {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
	        response.setContentType("application/json;charset=UTF-8");
	        response.getWriter().write(createErrorBody(exception));
		}
}

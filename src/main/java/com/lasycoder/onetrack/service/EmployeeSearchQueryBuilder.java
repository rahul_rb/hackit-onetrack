package com.lasycoder.onetrack.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.lasycoder.onetrack.exceptions.ClientException;
import com.lasycoder.onetrack.model.EmployeeRole;

public class EmployeeSearchQueryBuilder<T> implements Specification<T> {
	
	 
	private static final long serialVersionUID = 1L;
	private Map<String,Object> searchMap = new HashMap<>();
	private String filterType;
	
	public EmployeeSearchQueryBuilder(String filterType,Map<String,Object> map) throws Exception {
		this.filterType = filterType;
		mapRequestParamWithEntityKey(map);
		validatePreCheckBasedOnFilterType(this.filterType);
	}

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) { 
		List<Predicate> predicates = new ArrayList();  
		
		for(Entry<String,Object> entry : searchMap.entrySet()) { 
			if (entry.getKey().equals("projectId")) { 
				Join<Object,Object> joinData=  root.join("project",JoinType.INNER); 
				predicates.add(criteriaBuilder.equal(joinData.get(entry.getKey()),entry.getValue()));
			} else { 
				predicates.add(criteriaBuilder.equal(root.get(entry.getKey()),entry.getValue()));
			}  
		}  
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}
	
	
	private void validatePreCheckBasedOnFilterType(String filterType) throws Exception {
		switch(filterType) {
			case "project":
				if (!searchMap.containsKey("projectId")) {
					throw new ClientException("Project Id must be supplied for the filterType: " + filterType);
				}
				break; 
			case "manager":
				if (!searchMap.containsKey("managerId")) {
					throw new ClientException("ManagerId Id must be supplied for the filterType: " + filterType);
				}
				break;
			default:
				throw new ClientException("Filter Type " + filterType +  " not supported..");
		}
		
		
	}
	private void mapRequestParamWithEntityKey(Map<String,Object> map) throws Exception {
		for(Entry<String,Object> entry : map.entrySet()) { 
			switch (entry.getKey()) {
				case "employeeId":
					searchMap.put("userId", entry.getValue());
					break;
				case "projectId":
				case "projectName":
					searchMap.put("projectId", entry.getValue());
					break;
				case "managerId":
					searchMap.put("managerId", entry.getValue());
					break;
				case "fullName":
					searchMap.put("fullName", entry.getValue());
					break;
				case "employeeRole": 
					searchMap.put("employeeRole",EmployeeRole.valueOf(String.valueOf(entry.getValue())));
					break;
				default:
					throw new ClientException("Filter Query Key " + entry.getKey() + " does follow the search criteria");
					
			}
		}
	}

}

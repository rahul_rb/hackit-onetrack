/**
 * 
 */
package com.lasycoder.onetrack.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lasycoder.onetrack.dao.ProjectDAO;
import com.lasycoder.onetrack.model.Project;

/**
 * @author rbarahpu
 *
 */

@Service
public class ProjectService {
	
	@Autowired
	private ProjectDAO projectDao;
	
	public Project addProject(Project project) {
		return projectDao.save(project);
	}
	
	public Optional<Project> getProjectById(Long id) {
		return projectDao.findById(id);
	}
	
	public List<Project> getAllProjects() {
		return (List<Project>) projectDao.findAll();
	}
	
	public Optional<Project> getProjectByProjectId (String projectId) {
		return projectDao.findByProjectId(projectId);
	}
	
	public void deleteProjectById(Long id) {
		projectDao.deleteById(id);
	}
}

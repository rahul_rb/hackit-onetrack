package com.lasycoder.onetrack.service;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthorizationAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {

        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(createErrorBody(accessDeniedException));
    }

    private String createErrorBody(AccessDeniedException exception) throws JsonProcessingException {
    	Map<String,Object> exceptionMessage = new HashMap<>();
        exceptionMessage.put("code", HttpStatus.FORBIDDEN.value());
        exceptionMessage.put("reason", HttpStatus.FORBIDDEN.getReasonPhrase());
        exceptionMessage.put("timestamp", Instant.now().toString());
        exceptionMessage.put("message", exception.getMessage());
        return new ObjectMapper().writeValueAsString(exceptionMessage);
    }

}

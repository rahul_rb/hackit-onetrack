/**
 * 
 */
package com.lasycoder.onetrack.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.lasycoder.onetrack.dao.AllocationDAO;
import com.lasycoder.onetrack.dao.EmployeeDAO;
import com.lasycoder.onetrack.dao.InitiativeDAO;
import com.lasycoder.onetrack.dto.AllocationListResponse;
import com.lasycoder.onetrack.dto.AllocationRequest;
import com.lasycoder.onetrack.dto.AllocationResponse;
import com.lasycoder.onetrack.dto.EmployeeListResponse;
import com.lasycoder.onetrack.dto.EmployeeResponse;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.dto.InitiativeResponse;
import com.lasycoder.onetrack.dto.InitiativesEmployeeMapping;
import com.lasycoder.onetrack.dto.PaginatedList;
import com.lasycoder.onetrack.model.Allocation;
import com.lasycoder.onetrack.model.Employee;
import com.lasycoder.onetrack.model.EmployeeRole;
import com.lasycoder.onetrack.model.Initiative;

/**
 * @author rbarahpu
 *
 */
@Service
public class AllocationService {
	
	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Autowired
	private InitiativeDAO initiativeDAO;
	
	@Autowired
	private AllocationDAO allocationDAO;
	
	public List<Allocation> getAllAllocations() {
		return (List<Allocation>) allocationDAO.findAll();
	} 
	
	public void deleteAllocationById(Long id) {
		allocationDAO.deleteById(id);
	}
	
	public void deleteAllocation(Allocation allocation) {
		allocationDAO.delete(allocation);
	}
	
	public Optional<Allocation> getAllocationById(Long id) {
		return allocationDAO.findById(id);
	}
	
	public Optional<Allocation> getAllocationByEmployeeId(Long id) {
		return allocationDAO.findByEmployeeId(id);
	}
	
//	public Optional<Allocation> getAllocationByInitiativeId(Long id) {
//		return allocationDAO.findByInitiativeId(id);
//	}
 
	public Optional<Allocation> getAllocationByStartTime(Date date) {
		return allocationDAO.findByStartTime(date);
	}
	
	public Optional<Allocation> getAllocationByEndTime(Date date) {
		return allocationDAO.findByEndTime(date);
	}
	
	public AllocationResponse createAllocationByProject(String projectId,AllocationRequest allocationRequest,String principalUser) {
		AllocationResponse response = new AllocationResponse();
		if (allocationRequest.getStartTime()>allocationRequest.getEndTime()) {
			response.setResponseCode(HttpStatus.BAD_REQUEST);
			ErrorModel error = frameISEErrorModel("Start Time should be less than End Time",HttpStatus.BAD_REQUEST);
			response.setErrorModel(error);
			return response;
		}
		
		Optional<Employee> emp = employeeDAO.findByUserId(allocationRequest.getEmployeeUserId());
		if (emp.isPresent()) {
			Employee employee = emp.get();
			Optional<Initiative> ini = initiativeDAO.findByInitiativeId(allocationRequest.getInitiativeId()); 
			if (ini.isPresent()) {
				Initiative initiative = ini.get();
				if (projectId.equals(initiative.getProject().getProjectId())) {
					Date startDate = new Date(allocationRequest.getStartTime());
					Date endDate = new Date(allocationRequest.getEndTime());
					int allocationCount = 0;
					if (employee.getEmployeeRole().equals(EmployeeRole.EMPLOYEE)) {
						allocationCount = allocationCount = allocationDAO.existsByEmployeeIdAndStartTimeAndEndTime(employee.getId(), new Date(allocationRequest.getStartTime()),  new Date(allocationRequest.getEndTime()));
						
					} else {
						allocationCount = allocationCount = allocationDAO.existsByEmployeeIdAndInitiativeIdAndStartTimeAndEndTime(employee.getId(),initiative.getId(), new Date(allocationRequest.getStartTime()),  new Date(allocationRequest.getEndTime()));
					} 
					 
					if (allocationCount == 0) {
						Allocation allocation = new Allocation(employee.getId(),initiative.getId(),startDate,endDate);
						allocation.setCreatedTimestamp(System.currentTimeMillis()/1000);
						allocation.setCreatedBy(principalUser);
						
						allocation = allocationDAO.save(allocation);
						response.setAllocationId(String.valueOf(allocation.getId()));
						response.setSucessMsg("Successfully Allocation created");
						response.setResponseCode(HttpStatus.CREATED);
						return response;
					}  
					response.setResponseCode(HttpStatus.CONFLICT);
					ErrorModel error = frameISEErrorModel("Employee already part of another initiative on the given timeframe",HttpStatus.CONFLICT);
					response.setErrorModel(error);
					return response;
				}
				response.setResponseCode(HttpStatus.BAD_REQUEST);
				ErrorModel error = frameISEErrorModel("Initiative does not exists in belonging Project",HttpStatus.BAD_REQUEST);
				response.setErrorModel(error);
				return response;
 			}
			response.setResponseCode(HttpStatus.BAD_REQUEST);
			ErrorModel error = frameISEErrorModel("Initiative does not exists",HttpStatus.BAD_REQUEST);
			response.setErrorModel(error);
			return response;
		}
		response.setResponseCode(HttpStatus.BAD_REQUEST);
		ErrorModel error = frameISEErrorModel("Employee does not exists",HttpStatus.BAD_REQUEST);
		response.setErrorModel(error);
		
		return response;
	}
	
	public EmployeeListResponse getAvailableEmployeesOnGivenTimePerProject(String projectId, long dateStamp) { 
		List<Employee> employees = employeeDAO.findAvailableEmployeesBetweenStartTimeandEndTime(new Date(dateStamp),projectId,2);
		
		EmployeeListResponse avaialbleEmployeeList = new EmployeeListResponse();
		PaginatedList<EmployeeResponse> list = new PaginatedList();
		list.setResults(new ArrayList<EmployeeResponse>());		
		list.setTotalRecords(employees.size());
		list.setLimit(employees.size());
		
		employees.stream().map(emp -> {
			EmployeeResponse employee = new EmployeeResponse();
			employee.setEmployeeId(emp.getUserId());
			employee.setFullName(emp.getFullName());
			employee.setEmployeeRole(emp.getEmployeeRole().toString());
			employee.setManagerId(emp.getManagerId());
			employee.setProjectId(emp.getProject().getProjectId());
			return employee;
		}).forEach(empl -> {
			list.getResults().add(empl);
		});
		avaialbleEmployeeList.setEmployees(list);
		
		return avaialbleEmployeeList;
	} 
	
	public AllocationListResponse getAllocationsByTimeRangeEmployeeId(Date startTime, Date endTime, String userId,
			int offset, int limit) {

		AllocationListResponse response = new AllocationListResponse();
		Pageable pageable = PageRequest.of(offset, limit);

		if (userId != null) {
			Optional<Employee> employee = employeeDAO.findByUserId(userId);

			if (!employee.isPresent()) {
				ErrorModel errorModel = new ErrorModel();
				errorModel.setErrorMessage("Employee with userId " + userId + " not found");
				errorModel.setErrorCode(HttpStatus.BAD_REQUEST.toString());
				response.setError(errorModel);
				response.setResponseCode(HttpStatus.BAD_REQUEST);
				return response;
			}
			Long employeeId = employee.get().getId();
			Page<Allocation> allocationsPage = allocationDAO.findAllocationsByTimeRangeAndEmployeeId(startTime, endTime,
					employeeId, pageable);
			long totalRecords = allocationsPage.getTotalElements();
			List<Allocation> allocations = allocationsPage.toList();

			List<AllocationResponse> allocationResponseList = new ArrayList<AllocationResponse>();

			allocations.forEach(a -> {

				Long initiativeId = a.getInitiativeId();
				Optional<Initiative> optInitiative = initiativeDAO.findById(initiativeId);

				Initiative initiative = null;

				try {
					initiative = optInitiative.get();

				} catch (NoSuchElementException e) {
					throw new NoSuchElementException("Server error has occurred: Data inconsistent.");
				}

				AllocationResponse allocationResponse = new AllocationResponse(initiative.getInitiativeId(),
						initiative.getInitiativeDesc(), a.getStartTime(), a.getEndTime());

				allocationResponseList.add(allocationResponse);

			});

			PaginatedList<AllocationResponse> allocationResponsePageList = new PaginatedList<AllocationResponse>(
					totalRecords, offset, limit, allocationResponseList);
			response.setAllocations(allocationResponsePageList);
			return response;
		}

		Page<Allocation> allocationPage = allocationDAO.findAllocationsByTimeRange(startTime, endTime, pageable);

		List<Allocation> allocationList = allocationPage.toList();

		List<InitiativesEmployeeMapping> initiativesEmployeeMappings = new ArrayList<InitiativesEmployeeMapping>();

		allocationList.forEach(allocation -> {

			Long employeeId = allocation.getEmployeeId();

			List<Initiative> initiativeList = initiativeDAO.findByEmployeeId(employeeId);
			List<InitiativeResponse> initiativeResponseList = new ArrayList<InitiativeResponse>();

			initiativeList.forEach(i -> {
				InitiativeResponse ir = new InitiativeResponse(i.getId(), i.getInitiativeId(), i.getInitiativeDesc(),
						i.getStatus(), i.getCreatedTimestamp());
 
				
				Optional<Allocation> optAllocation = allocationDAO.findByInitiativeIdAndAllocationId(i.getId(), allocation.getId());
 
				ir.setStartTime(optAllocation.get().getStartTime());
				ir.setEndTime(optAllocation.get().getEndTime());
				initiativeResponseList.add(ir);

			});

			Optional<Employee> optEmployee = employeeDAO.findById(employeeId);
			Employee employee = null;
			try {
				employee = optEmployee.get();

			} catch (NoSuchElementException e) {

				throw new NoSuchElementException("Server error has occurred: Data Inconsistent - " + e.getMessage());
			}
			Optional<Employee> manager = employeeDAO.findById(employee.getManagerId());
			
			
			EmployeeResponse er = new EmployeeResponse();
			try {
				er.setManagerName(manager.get().getFullName());
				er.setManagerUserId(manager.get().getUserId());
				
			} catch (NoSuchElementException e) {
				throw new NoSuchElementException("Server error has occurred: Data Inconsistent - " + e.getMessage());
			}
			er.setEmployeeId(employee.getUserId());
			er.setEmployeeRole(employee.getEmployeeRole().toString());
			er.setManagerId(employee.getManagerId());
			
			er.setFullName(employee.getFullName());

			InitiativesEmployeeMapping initiativesEmployeeMapping = new InitiativesEmployeeMapping();
			initiativesEmployeeMapping.setEmployeeResponse(er);
			initiativesEmployeeMapping.setInitiativeResponseList(initiativeResponseList);

			initiativesEmployeeMappings.add(initiativesEmployeeMapping);

		});

		response.setInitiativesEmployeeMappings(initiativesEmployeeMappings);

		return response;
	}
	
	private ErrorModel frameISEErrorModel(String message, HttpStatus status) {
		ErrorModel errorModel = new ErrorModel();
		errorModel.setErrorCode(status.toString());
		errorModel.setErrorMessage(
				message);
		return errorModel;
	}
	
	
}

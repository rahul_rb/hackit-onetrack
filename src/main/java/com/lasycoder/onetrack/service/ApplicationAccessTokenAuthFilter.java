package com.lasycoder.onetrack.service;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.lasycoder.onetrack.dto.ApplicationTokenDTO;
import com.lasycoder.onetrack.exceptions.BearerTokenAuthException;

public class ApplicationAccessTokenAuthFilter extends AbstractAuthenticationProcessingFilter {

	Logger logger = LoggerFactory.getLogger(getClass());

	private AuthenticationService authenticationService;
	 

	public ApplicationAccessTokenAuthFilter(AuthenticationService authService,
			AuthenticationManager authenticationManager,AuthenticationFailureHandler unauthHandler) {
		super("/api/v1/**", authenticationManager);
		this.authenticationService = authService; 
		this.setAuthenticationFailureHandler(unauthHandler);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		logger.info("Authentication Request made for url :: " + request.getRequestURI());

		String token = extarctAuthorizationHeader(request); 
		
		token  = token.replace("Bearer ", "");

		ApplicationTokenDTO accessTokenBody = authenticationService.validateApplicationAccessToken(token); 
		
		//GrantedAuthority auth = new SimpleGrantedAuthority(accessTokenBody.getRole()); 
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(accessTokenBody,
				null, Arrays.asList(accessTokenBody.getAuthorites()));
		return this.getAuthenticationManager().authenticate(authentication); 
	}
	
	@Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

		logger.info("Successfully authentication for the request {}", request.getRequestURI());

        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }

	private String extarctAuthorizationHeader(HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			if (token == null) throw new BearerTokenAuthException("No Token provided in the request");
			return token;
		} catch (Exception ex) {
			throw new BearerTokenAuthException("No Token provided in the request");
		}
	}

}

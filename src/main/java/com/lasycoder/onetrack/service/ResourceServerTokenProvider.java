package com.lasycoder.onetrack.service;

import com.lasycoder.onetrack.dto.ResoruceAuthToken;

public interface ResourceServerTokenProvider {
	public abstract ResoruceAuthToken generateAccessTokenWithAuthorizationCode(String authorizationCode, String state);
	
	public abstract String contructURIForAuthentication(); 
	
	public abstract String generateAccessTokenWithRefreshToken(String refreshToken);
}

package com.lasycoder.onetrack.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lasycoder.onetrack.dao.EmployeeDAO;
import com.lasycoder.onetrack.dto.ApplicationTokenDTO;
import com.lasycoder.onetrack.dto.ResoruceAuthToken;
import com.lasycoder.onetrack.dto.ResourceUserDetails;
import com.lasycoder.onetrack.exceptions.OAuth2Exception;
import com.lasycoder.onetrack.exceptions.UserNotFoundException;
import com.lasycoder.onetrack.model.Employee;
import com.lasycoder.onetrack.util.JWTUtility;

@Service
public class AuthenticationService {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ResourceAPIProvider apiProviderClient;
	
	@Autowired
	ResourceServerTokenProvider tokenProviderClient;
	
	@Autowired
	EmployeeDAO employeeDao;
	
	@Autowired
	JWTUtility jwtUtility;

	public ApplicationTokenDTO generateApplicationAccessToken(String authorizaCode, String state) throws Exception { 
		ResoruceAuthToken resouceToken = tokenProviderClient.generateAccessTokenWithAuthorizationCode(authorizaCode, state);
		
		if (resouceToken!=null) {
			logger.info("Successfully granted the authorization code from Token provider");
			ResourceUserDetails selfUserDetails = apiProviderClient.fetchUserDetails(resouceToken.getAccess_token(), null);
			if (selfUserDetails!=null) {
				logger.info("Successfully fetched Self User Details from Api Server");
				String userId = selfUserDetails.getUserId();
				Optional<Employee> emp = employeeDao.findByUserId(userId);
				if (emp.isPresent()) {
					logger.info("User is already registered in system");
					Employee employee = emp.get();
					Map<String,Object> accessTokenDetails = new HashMap();
					
					accessTokenDetails.put("userId", userId);  
					accessTokenDetails.put("role", employee.getEmployeeRole().toString());
					accessTokenDetails.put("managerId", employee.getManagerId());
					accessTokenDetails.put("emailId", employee.getEmailAddress());
					accessTokenDetails.put("projectId", employee.getProject().getProjectId());
					accessTokenDetails.put("tokenProviderObject", resouceToken);
					
					String applicationAccessToken = jwtUtility.generateAccessToken(accessTokenDetails);
					
					
					ApplicationTokenDTO applicationDTO = new ApplicationTokenDTO();
					applicationDTO.setAccessToken(applicationAccessToken);
					applicationDTO.setUserId(userId);
					applicationDTO.setEmailId(employee.getEmailAddress());
					applicationDTO.setRole(employee.getEmployeeRole().toString()); 
					applicationDTO.setManagerId(String.valueOf(employee.getManagerId()));
					applicationDTO.setProjectId(employee.getProject().getProjectId());
					
					logger.info("Successfully generated application access token");
					return applicationDTO;
				} else {
					throw new UserNotFoundException("User not found in our system. Please Check with your Adminstrator");
				}
			} else {
				throw new UserNotFoundException("No User found from API Server");
			}
		} else {
			throw new OAuth2Exception("Unable to authenticate with Token Provider");
		} 
	}
	
	public ApplicationTokenDTO validateApplicationAccessToken(String accessToken) {
		Map<String,Object> parsedAccessToken =  jwtUtility.validateJwtToken(accessToken);
		
		ApplicationTokenDTO applicationDTO = new ApplicationTokenDTO();
		 
		applicationDTO.setUserId((String) parsedAccessToken.get("sub"));
		applicationDTO.setEmailId((String) parsedAccessToken.get("emailId"));
		applicationDTO.setRole((String) parsedAccessToken.get("userRole"));  
		applicationDTO.setManagerId(String.valueOf(parsedAccessToken.get("managerId")));
		applicationDTO.setTokenObject((ResoruceAuthToken) parsedAccessToken.get("tokenObject"));
		applicationDTO.setProjectId((String) parsedAccessToken.get("projectId"));
		
		return applicationDTO;
	}
	
	public String  generateOAuth2RedrectUrl(String userId) throws Exception {
		Optional<Employee> emp = employeeDao.findByUserId(userId);
		if (emp.isPresent()) {
			return tokenProviderClient.contructURIForAuthentication();
		}
		throw new UserNotFoundException("User not found in our system. Please Check with your Adminstrator");
	}
}

package com.lasycoder.onetrack.service;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.lasycoder.onetrack.config.CIAuthConfig;
import com.lasycoder.onetrack.dto.ResoruceAuthToken;
import com.lasycoder.onetrack.dto.ResourceUserDetails;

@Service
public class CIClient implements ResourceServerTokenProvider, ResourceAPIProvider {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	CIAuthConfig ciAuthConfig;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public ResourceUserDetails fetchUserDetails(String accessToken, String userId) {
		if (accessToken == null)
			return null;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.setBearerAuth(accessToken);

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity(null, headers);

		String url = "";
		if (userId != null) {
			url = ciAuthConfig.getUrlDomain() + "/v1/people?email=" + userId;
		} else {
			url = ciAuthConfig.getUrlDomain() + "/v1/people/me";
		}

		ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Map.class);

		if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
			Map<String, Object> responseBody = responseEntity.getBody();

			ResourceUserDetails userDetails = new ResourceUserDetails();
			String emailId = (String) ((ArrayList) responseBody.get("emails")).get(0);
			userDetails.setUserId(emailId);
			userDetails.setEmailId(emailId);
			userDetails.setDisplayName((String) responseBody.get("displayName"));
			userDetails.setAvatar((String) responseBody.get("avatar"));

			return userDetails;
		} else {
			return null;
		}
		
	}

	@Override
	public ResoruceAuthToken generateAccessTokenWithAuthorizationCode(String authorizationCode, String state) {
		if (!ciAuthConfig.getState().equals(state))
			return null;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap();
		
	
		paramMap.add("grant_type", ciAuthConfig.getGrantType());
		paramMap.add("client_id", ciAuthConfig.getClientid());
		paramMap.add("code", authorizationCode);
		paramMap.add("client_secret", ciAuthConfig.getClientsecret());
		paramMap.add("redirect_uri", ciAuthConfig.getRedirectUrl());

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity(paramMap, headers);

		ResponseEntity<ResoruceAuthToken> responseEntity = restTemplate.exchange(
				ciAuthConfig.getUrlDomain() + ciAuthConfig.getTokenUriPath(), HttpMethod.POST, requestEntity,
				ResoruceAuthToken.class);
		if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
			return responseEntity.getBody();
		} else {
			return null;
		}
		
	}

	@Override
	public String contructURIForAuthentication() {
		return ciAuthConfig.constructURI();
	}

	@Override
	public String generateAccessTokenWithRefreshToken(String refreshToken) {
		// TODO Auto-generated method stub
		return null;
	}

}

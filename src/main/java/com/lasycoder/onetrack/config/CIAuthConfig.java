package com.lasycoder.onetrack.config;

import java.net.URLEncoder;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app.auth.ci")
public class CIAuthConfig {
	private String clientid;
	private String clientsecret;
	private String urlDomain;
	private String grantType;
	private String tokenUriPath;
	private String fetchUserInfo;
	private String scope;
	private String state;
	private String authorizePath;
	private String redirectUrl;
	
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public String getAuthorizePath() {
		return authorizePath;
	}
	public void setAuthorizePath(String authorizePath) {
		this.authorizePath = authorizePath;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getClientid() {
		return clientid;
	}
	public void setClientid(String clientId) {
		this.clientid = clientId;
	}
	public String getClientsecret() {
		return clientsecret;
	}
	public void setClientsecret(String clientSecret) {
		this.clientsecret = clientSecret;
	}
	public String getUrlDomain() {
		return urlDomain;
	}
	public void setUrlDomain(String urlDomain) {
		this.urlDomain = urlDomain;
	}
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getTokenUriPath() {
		return tokenUriPath;
	}
	public void setTokenUriPath(String tokenUriPath) {
		this.tokenUriPath = tokenUriPath;
	}
	public String getFetchUserInfo() {
		return fetchUserInfo;
	}
	public void setFetchUserInfo(String fetchUserInfo) {
		this.fetchUserInfo = fetchUserInfo;
	}
	
	
	public String constructURI() {
		StringBuilder urlForAuthentication = new StringBuilder("");
		String encodedChar = URLEncoder.encode(getScope()).replaceAll("%20", "+");
		
		urlForAuthentication.append(getUrlDomain()).append(getAuthorizePath())
				.append("?response_type=").append("code")
				.append("&client_id=").append(getClientid())
				.append("&state=").append(getState())
				.append("&redirect_uri=").append(URLEncoder.encode(getRedirectUrl()))
				.append("&scope=").append(encodedChar);
		return urlForAuthentication.toString();
	}
}

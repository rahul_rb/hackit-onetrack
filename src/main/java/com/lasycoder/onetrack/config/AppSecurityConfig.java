package com.lasycoder.onetrack.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.lasycoder.onetrack.service.AccessTokenAuthenticationFailureHandler;
import com.lasycoder.onetrack.service.ApplicationAccessTokenAuthFilter;
import com.lasycoder.onetrack.service.AuthenticationService;
import com.lasycoder.onetrack.service.AuthorizationAccessDeniedHandler;
import com.lasycoder.onetrack.service.CustomAuthProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity( 
		prePostEnabled = true)
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	AuthenticationService authenticationService;
	
	@Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new CustomAuthProvider();
    }
	 
    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new AccessTokenAuthenticationFailureHandler();
    }
	
	@ConditionalOnMissingBean
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .cors()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
				.authorizeRequests().antMatchers("/api/auth/**").permitAll();
         
         http.addFilterBefore(
        		 new ApplicationAccessTokenAuthFilter(authenticationService,authenticationManagerBean(),authenticationFailureHandler()),
                        BasicAuthenticationFilter.class);
    }
	
	 	@Bean
	    public AccessDeniedHandler accessDeniedHandler() {
	        return new AuthorizationAccessDeniedHandler();
	    }

}

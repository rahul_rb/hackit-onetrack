package com.lasycoder.onetrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneTrackApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneTrackApplication.class, args);
	}

}

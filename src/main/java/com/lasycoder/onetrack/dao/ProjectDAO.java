/**
 * 
 */
package com.lasycoder.onetrack.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lasycoder.onetrack.model.Project;

/**
 * @author rbarahpu
 *
 */

@Repository
public interface ProjectDAO extends CrudRepository<Project, Long>{

	Optional<Project> findByProjectId(String projectId);
	
}

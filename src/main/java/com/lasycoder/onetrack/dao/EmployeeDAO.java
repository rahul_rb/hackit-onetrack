/**
 * 
 */
package com.lasycoder.onetrack.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lasycoder.onetrack.model.Employee;

/**
 * @author rbarahpu
 *
 */

@Repository
public interface EmployeeDAO extends PagingAndSortingRepository<Employee, Long>, JpaSpecificationExecutor<Employee>{

	void deleteByUserId(String userId);

	Optional<Employee> findByUserId(String userId);
	
	Optional<Employee> findByManagerId(long managerId);
	
	Optional<Employee> findByEmailAddress(String emailAddress);
	@Query(nativeQuery = true,
			value="select * from employee where id not in ("
					+ "select distinct employee_id from allocation al  "
					+ "					inner join employee em on em.id=al.employee_id and em.role=?3 "
					+ "            where ?1  between start_time AND end_time) and project_id=?2 ")
	List<Employee> findAvailableEmployeesBetweenStartTimeandEndTime(Date currentStamp,String projectId,int employeeRole);
	
	 

}

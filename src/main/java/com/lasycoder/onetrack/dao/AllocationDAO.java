package com.lasycoder.onetrack.dao;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lasycoder.onetrack.model.Allocation;

/**
 * @author rbarahpu
 *
 */

@Repository
public interface AllocationDAO extends CrudRepository<Allocation, Long> {

	Optional<Allocation> findByEmployeeId(Long id);
	
	@Query(value =  "SELECT * FROM allocation WHERE initiative_id=?1 AND id=?2", nativeQuery = true)
	Optional<Allocation> findByInitiativeIdAndAllocationId(Long initiativeId, Long allocationId);

	Optional<Allocation> findByStartTime(Date date);

	Optional<Allocation> findByEndTime(Date date);
	
	@Query(nativeQuery = true,
			value="SELECT count(*) FROM allocation WHERE employee_id=?1 AND ((?2 BETWEEN start_time AND end_time) OR (?3 BETWEEN start_time AND end_time))")
	int existsByEmployeeIdAndStartTimeAndEndTime(Long employeeId,Date startTime,Date endTime);
	
	@Query(nativeQuery = true,
			value="SELECT count(*) FROM allocation WHERE employee_id=?1 AND initiative_id=?2 AND ((?3 BETWEEN start_time AND end_time) OR (?4 BETWEEN start_time AND end_time))")
	int existsByEmployeeIdAndInitiativeIdAndStartTimeAndEndTime(Long employeeId,Long initId,Date startTime,Date endTime);
	
	
	@Query(value = "SELECT * FROM allocation WHERE (?1 BETWEEN start_time AND end_time) OR (?2 BETWEEN start_time AND end_time)", nativeQuery = true)
	Page<Allocation> findAllocationsByTimeRange(Date startTime, Date endTime, Pageable pageable);
	
	
	@Query(value = "SELECT * FROM allocation WHERE ((?1 BETWEEN start_time AND end_time) OR (?2 BETWEEN start_time AND end_time)) AND employee_id=?3", nativeQuery = true)
	Page<Allocation> findAllocationsByTimeRangeAndEmployeeId(Date startTime, Date endTime, Long employeeId, Pageable pageable);
	
	

}

 
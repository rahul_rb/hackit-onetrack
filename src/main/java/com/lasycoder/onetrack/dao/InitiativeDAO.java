/**
 * 
 */
package com.lasycoder.onetrack.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lasycoder.onetrack.model.Initiative;

/**
 * @author rbarahpu
 *
 */

@Repository
public interface InitiativeDAO extends PagingAndSortingRepository<Initiative, Long>{

	Optional<Initiative> findByInitiativeId(String initiativeId);
	
	@Query(value = "SELECT * FROM initiative WHERE project_id=?1", nativeQuery = true)
	Page<Initiative> findByProjectId(String projectId, Pageable pageable);
	
	@Query(value = "SELECT  i.* FROM initiative i INNER JOIN allocation a on i.id = a.initiative_id WHERE a.employee_id=?1", nativeQuery = true)
	List<Initiative> findByEmployeeId(Long employeeId);
	
	
	

}

package com.lasycoder.onetrack.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lasycoder.onetrack.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	
	public Optional<Employee> findByUserId(String userId);

}

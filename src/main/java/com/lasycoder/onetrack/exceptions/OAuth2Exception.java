package com.lasycoder.onetrack.exceptions;

public class OAuth2Exception extends Exception {
	 
	public OAuth2Exception(String message) {
		super(message);
	}
}

package com.lasycoder.onetrack.exceptions;

import org.springframework.security.core.AuthenticationException;

public class BearerTokenAuthException extends AuthenticationException {

	public BearerTokenAuthException(String msg) {
		super(msg); 
	}

}

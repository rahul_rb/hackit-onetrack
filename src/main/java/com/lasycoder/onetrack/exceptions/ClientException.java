package com.lasycoder.onetrack.exceptions;

public class ClientException extends Exception {
	public ClientException(String message) {
		super(message);
	}
}

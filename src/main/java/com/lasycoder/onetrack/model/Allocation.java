/**
 * 
 */
package com.lasycoder.onetrack.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author rbarahpu
 *
 */

@Entity
@Table(name = "allocation")
public class Allocation {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "id")
	private Long id; 

	@Column(name = "employee_id")
	private Long employeeId;
	
	@Column(name = "initiative_id")
	private Long initiativeId;
	
	private Date startTime;
	
	private Date endTime;
	
	@Column(name = "created_timestamp")
	private Long createdTimestamp;
	
	@Column(name = "created_by",length = 255)
	private String createdBy;
	
	public Allocation() {
		
	}
	
	public Allocation(Long employeeId, Long initiativeId, Date startTime, Date endTime) {
		super();
		this.employeeId = employeeId;
		this.initiativeId = initiativeId;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getInitiativeId() {
		return initiativeId;
	}

	public void setInitiativeId(Long initiativeId) {
		this.initiativeId = initiativeId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public Long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
}

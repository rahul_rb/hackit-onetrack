/**
 * 
 */
package com.lasycoder.onetrack.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author rbarahpu
 *
 */
@Entity
@Table(name = "project")
public class Project implements Serializable  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "project_id")
	private String projectId;
	
	@Column(name = "project_desc")
	private String projectDesc;
	
	@Column(name = "created_timestamp")
	private long createdTimestamp;
	
	@Column(name = "created_by",length = 255)
	private String createdBy;

	
	public Project(String projectId, String projectDesc) {
		super();
		this.projectId = projectId;
		this.projectDesc = projectDesc;
	}

	public Project(String projectId) {
		super();
		this.projectId = projectId;
	}

	public Project(long id, String projectId, String projectDesc, long createdTimestamp, String createdBy) {
		super();
		this.id = id;
		this.projectId = projectId;
		this.projectDesc = projectDesc;
		this.createdTimestamp = createdTimestamp;
		this.createdBy = createdBy;
	}

	public String getProjectDesc() {
		return projectDesc;
	}

	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}

	public Project() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	

}

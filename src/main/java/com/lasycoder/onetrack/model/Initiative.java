/**
 * 
 */
package com.lasycoder.onetrack.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * @author rbarahpu
 *
 */
@Entity
@Table(name = "initiative")
public class Initiative implements Serializable { 

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "id")
	private long id;
	
	
	@Column(name = "initiative_id")
	@NotBlank(message = "Empty InI Id")
	public String initiativeId;
	
	
	@Column(name = "initiative_desc")
	@NotBlank(message = "Empty InI des")
	public String initiativeDesc;
	
	@Column(name = "status")
	private String status;
	
	@ManyToOne
	@JoinColumn(name = "project_id", referencedColumnName = "project_id")
	private Project project;
	
	@Column(name = "created_timestamp")
	private long createdTimestamp;
	
	@Column(name = "created_by",length = 255)
	private String createdBy;

	public Initiative() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Initiative(String initiativeId, String initiativeDesc, Project project, String status, long createdTimestamp, String createdBy) {
		super();
		this.initiativeId = initiativeId;
		this.initiativeDesc = initiativeDesc;
		this.project = project;
		this.status = status;
		this.createdTimestamp = createdTimestamp;
		this.createdBy = createdBy;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInitiativeId() {
		return initiativeId;
	}

	public void setInitiativeId(String initiativeId) {
		this.initiativeId = initiativeId;
	}

	public String getInitiativeDesc() {
		return initiativeDesc;
	}

	public void setInitiativeDesc(String initiativeDesc) {
		this.initiativeDesc = initiativeDesc;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

}

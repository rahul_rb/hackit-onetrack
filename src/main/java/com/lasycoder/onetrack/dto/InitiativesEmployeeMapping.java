package com.lasycoder.onetrack.dto;

import java.util.List;

import com.lasycoder.onetrack.model.Employee;

public class InitiativesEmployeeMapping {
	
	private List<InitiativeResponse> initiativeResponseList;
	
	private EmployeeResponse employeeResponse;

	public List<InitiativeResponse> getInitiativeResponseList() {
		return initiativeResponseList;
	}

	public void setInitiativeResponseList(List<InitiativeResponse> initiativeResponseList) {
		this.initiativeResponseList = initiativeResponseList;
	}

	public EmployeeResponse getEmployeeResponse() {
		return employeeResponse;
	}

	public void setEmployeeResponse(EmployeeResponse employeeResponse) {
		this.employeeResponse = employeeResponse;
	}

	public InitiativesEmployeeMapping(List<InitiativeResponse> initiativeResponseList,
			EmployeeResponse employeeResponse) {
		super();
		this.initiativeResponseList = initiativeResponseList;
		this.employeeResponse = employeeResponse;
	}

	public InitiativesEmployeeMapping() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
}

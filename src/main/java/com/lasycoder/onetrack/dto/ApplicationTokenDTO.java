package com.lasycoder.onetrack.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_EMPTY)
public class ApplicationTokenDTO {
	private String accessToken;
	private String userId;
	private String emailId;
	private String role;
	private String managerId;
	private String projectId;
	
	@JsonIgnore
	private ResoruceAuthToken tokenObject;
	
	

	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role; 
	}
	 
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
	@JsonIgnore
	public ResoruceAuthToken getTokenObject() {
		return tokenObject;
	}

	public void setTokenObject(ResoruceAuthToken tokenObject) {
		this.tokenObject = tokenObject;
	}
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	
	public GrantedAuthority getAuthorites() {
		GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role); 
		return authority;
	} 
	
}

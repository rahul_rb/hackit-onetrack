package com.lasycoder.onetrack.dto;

public class EmployeeIdAllocationsMapping {
	
	private Long employeeId;
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	private InitiativesEmployeeMapping initiativesEmployeeDTO;

	public InitiativesEmployeeMapping getInitiativesEmployeeDTO() {
		return initiativesEmployeeDTO;
	}

	public void setInitiativesEmployeeDTO(InitiativesEmployeeMapping initiativesEmployeeDTO) {
		this.initiativesEmployeeDTO = initiativesEmployeeDTO;
	}
	
	
}

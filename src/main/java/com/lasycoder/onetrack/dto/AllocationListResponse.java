package com.lasycoder.onetrack.dto;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class AllocationListResponse {
	
	PaginatedList<AllocationResponse> allocations;
	ErrorModel error;
	HttpStatus responseCode;
	
	private List<InitiativesEmployeeMapping> initiativesEmployeeMappings;
	
	
	public List<InitiativesEmployeeMapping> getInitiativesEmployeeMappings() {
		return initiativesEmployeeMappings;
	}
	public void setInitiativesEmployeeMappings(List<InitiativesEmployeeMapping> initiativesEmployeeMappings) {
		this.initiativesEmployeeMappings = initiativesEmployeeMappings;
	}
	PaginatedList<EmployeeIdAllocationsMapping> employeeAllocationsMappings;
	
	public PaginatedList<EmployeeIdAllocationsMapping> getEmployeeAllocationsMappings() {
		return employeeAllocationsMappings;
	}
	public void setEmployeeAllocationsMappings(PaginatedList<EmployeeIdAllocationsMapping> employeeAllocationsMappings) {
		this.employeeAllocationsMappings = employeeAllocationsMappings;
	}
	public PaginatedList<AllocationResponse> getAllocations() {
		return allocations;
	}
	public void setAllocations(PaginatedList<AllocationResponse> allocations) {
		this.allocations = allocations;
	}
	public ErrorModel getError() {
		return error;
	}
	public void setError(ErrorModel error) {
		this.error = error;
	}
	public HttpStatus getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	} 
	
	
	
	
}

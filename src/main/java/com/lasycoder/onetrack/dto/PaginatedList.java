package com.lasycoder.onetrack.dto;

import java.util.List;

public class PaginatedList<T> {
	
	public PaginatedList(long totalRecords, int offset, int limit, List<T> results) {
		super();
		this.totalRecords = totalRecords;
		this.offset = offset;
		this.limit = limit;
		this.results = results;
	}

	private long totalRecords;
	private int offset;
	private int limit;
	
	List<T> results;

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public PaginatedList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
}

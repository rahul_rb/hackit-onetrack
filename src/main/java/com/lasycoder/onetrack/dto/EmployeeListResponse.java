package com.lasycoder.onetrack.dto;

 
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class EmployeeListResponse  {
	PaginatedList<EmployeeResponse> employees;
	ErrorModel error;
	HttpStatus responseCode; 
	
	
	public HttpStatus getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	}
	public PaginatedList<EmployeeResponse> getEmployees() {
		return employees;
	}
	public void setEmployees(PaginatedList<EmployeeResponse> employees) {
		this.employees = employees;
	}
	public ErrorModel getError() {
		return error;
	}
	public void setError(ErrorModel error) {
		this.error = error;
	}
	
}

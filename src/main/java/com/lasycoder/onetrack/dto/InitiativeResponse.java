/**
 * 
 */
package com.lasycoder.onetrack.dto;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author rbarahpu
 *
 */
@JsonInclude(value = Include.NON_NULL)
public class InitiativeResponse {
	
	private Long initiativeId;
	private String successMsg;
	private HttpStatus responseCode;
	private ErrorModel errorModel;
	private String initiativeStringId;
	private String initiativeDesc;
	private String projectId;
	private String status;
	private Long createdTimestamp;
	
	private Date startTime;
	private Date endTime;
	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public InitiativeResponse() {}
	
	public InitiativeResponse(Long initiativeId, String initiativeStringId, String initiativeDesc, String projectId, String status,
			Long createdTimestamp) {
		super();
		this.initiativeStringId = initiativeStringId;
		this.initiativeDesc = initiativeDesc;
		this.projectId = projectId;
		this.status = status;
		this.createdTimestamp = createdTimestamp;
		this.initiativeId = initiativeId;
	}
	public InitiativeResponse(Long initiativeId, String initiativeStringId, String initiativeDesc, String status,
			long createdTimestamp) {
		// TODO Auto-generated constructor stub
		this.initiativeId = initiativeId;
		this.initiativeStringId = initiativeStringId;
		this.initiativeDesc = initiativeDesc;
		this.status = status;
		this.createdTimestamp = createdTimestamp;
	}

	public String getInitiativeStringId() {
		return initiativeStringId;
	}
	public void setInitiativeStringId(String initiativeStringId) {
		this.initiativeStringId = initiativeStringId;
	}
	public String getInitiativeDesc() {
		return initiativeDesc;
	}
	public void setInitiativeDesc(String initiativeDesc) {
		this.initiativeDesc = initiativeDesc;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public ErrorModel getErrorModel() {
		return errorModel;
	}
	public void setErrorModel(ErrorModel errorModel) {
		this.errorModel = errorModel;
	}
	public HttpStatus getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	}
	
	public Long getInitiativeId() {
		return initiativeId;
	}
	public void setInitiativeId(Long initiativeId) {
		this.initiativeId = initiativeId;
	}
	public String getSuccessMsg() {
		return successMsg;
	}
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}
	
	
	
	
	
}

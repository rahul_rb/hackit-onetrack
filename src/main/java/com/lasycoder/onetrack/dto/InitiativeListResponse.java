/**
 * 
 */
package com.lasycoder.onetrack.dto;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author rbarahpu
 *
 */

@JsonInclude(value = Include.NON_NULL)
public class InitiativeListResponse {
	
	private PaginatedList<InitiativeResponse> initiatives;
	private ErrorModel errorModel;
	private HttpStatus responseCode;
	
	public HttpStatus getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	}
	
	public PaginatedList<InitiativeResponse> getInitiatives() {
		return initiatives;
	}
	public void setInitiatives(PaginatedList<InitiativeResponse> initiatives) {
		this.initiatives = initiatives;
	}
	public ErrorModel getErrorModel() {
		return errorModel;
	}
	public void setErrorModel(ErrorModel errorModel) {
		this.errorModel = errorModel;
	}
	
	
}

package com.lasycoder.onetrack.dto;

import javax.validation.constraints.NotBlank;

public class AllocationRequest {
	@NotBlank(message = "Employee UserId is missing")
	private String employeeUserId;
	@NotBlank(message = "Initiative Id is missing")
	private String initiativeId;
	
	private long startTime;
	 
	private long endTime;
	
	public String getEmployeeUserId() {
		return employeeUserId;
	}
	public void setEmployeeUserId(String employeeId) {
		this.employeeUserId = employeeId;
	}
	public String getInitiativeId() {
		return initiativeId;
	}
	public void setInitiativeId(String initiativeId) {
		this.initiativeId = initiativeId;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	
	
}

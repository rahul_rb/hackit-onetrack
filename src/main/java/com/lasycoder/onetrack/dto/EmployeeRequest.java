package com.lasycoder.onetrack.dto;

public class EmployeeRequest {
	private String employeeFullName;
	private String emailAddress;
	private String employeeRole;
	private long managerId;
	private String projectId;

	public String getEmployeeFullName() {
		return employeeFullName;
	}

	public void setEmployeeFullName(String employeeFullName) {
		this.employeeFullName = employeeFullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}

	public long getManagerId() {
		return managerId;
	}

	public void setManagerId(long managerId) {
		this.managerId = managerId;
	}
	
	public String getProjectId() {
		return projectId;
	}

	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String validate() {
		if (employeeFullName == null || employeeFullName.trim().length() == 0) {
			return "Please enter valid employee full name";
		}
		if (emailAddress == null || emailAddress.trim().length() == 0) {
			return "Please enter valid employee emailaddress";
		}
		if (employeeRole == null || employeeRole.trim().length() == 0) {
			return "Please enter valid employee role. Only EMPLOYEE and MANAGER allowed";
		}
		 
		return null;
	}

}

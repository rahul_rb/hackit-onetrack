package com.lasycoder.onetrack.dto;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class AllocationResponse {
	
	public AllocationResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String sucessMsg;
	
	private String allocationId;
	
	private HttpStatus responseCode;
	
	private ErrorModel errorModel;
	
	private Long id;
	
	private Long employeeId;
	
	private Long managerId;
	
	private String managerName;
	
	public AllocationResponse(String initiativeStringId, String initiativeDesc, Date startTime, Date endTime) {
		super();
		this.initiativeStringId = initiativeStringId;
		this.initiativeDesc = initiativeDesc;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	private String initiativeStringId;
	
	private String initiativeDesc;
	
	private Date startTime;
	
	private Date endTime;
	
	public String getInitiativeStringId() {
		return initiativeStringId;
	}
	public void setInitiativeStringId(String initiativeStringId) {
		this.initiativeStringId = initiativeStringId;
	}
	public String getInitiativeDesc() {
		return initiativeDesc;
	}
	public void setInitiativeDesc(String initiativeDesc) {
		this.initiativeDesc = initiativeDesc;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public Long getManagerId() {
		return managerId;
	}
	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}
	public String getSucessMsg() {
		return sucessMsg;
	}
	public void setSucessMsg(String sucessMsg) {
		this.sucessMsg = sucessMsg;
	}
	public String getAllocationId() {
		return allocationId;
	}
	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
	
	public HttpStatus getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	}
	public ErrorModel getErrorModel() {
		return errorModel;
	}
	public void setErrorModel(ErrorModel errorModel) {
		this.errorModel = errorModel;
	}
}

/**
 * 
 */
package com.lasycoder.onetrack.dto;

import javax.validation.constraints.NotBlank;

/**
 * @author rbarahpu
 *
 */

public class InitiativeRequest {
	
	@NotBlank(message = "Initiative Id is missing")
	private String initiativeId;
	
	@NotBlank(message = "Initiative Description is missing")
	private String initiativeDesc;
	
	@NotBlank(message = "Project Id is missing")
	private String projectId;
	
	private String status;
	
	 
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getInitiativeId() {
		return initiativeId;
	}
	public void setInitiativeId(String initiativeId) {
		this.initiativeId = initiativeId;
	}
	public String getInitiativeDesc() {
		return initiativeDesc;
	}
	public void setInitiativeDesc(String initiativeDesc) {
		this.initiativeDesc = initiativeDesc;
	}
	public String getStatus() { 
		return status;
	}
	
	
}

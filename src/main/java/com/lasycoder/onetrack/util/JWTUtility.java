package com.lasycoder.onetrack.util;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lasycoder.onetrack.config.JwtConfig;
import com.lasycoder.onetrack.exceptions.BearerTokenAuthException;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JWTUtility {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	JwtConfig jwtConfig;
	
	
	public String  generateAccessToken(Map<String,Object> details) {
		
		 return Jwts.builder()
				.setSubject((String)details.get("userId"))
				.claim("userRole", details.get("role"))
				.claim("managerId", details.get("managerId"))
				.claim("emailId", details.get("emailId"))
				.claim("tokenProvider", "CI")
				.claim("projectId", details.get("projectId"))
				.claim("tokenProviderObject", details.get("tokenProviderObject")) 
				.setIssuedAt(new Date()) 
				.setExpiration(new Date((new Date()).getTime() + (Long.parseLong(jwtConfig.getTokenExpiry())*1000)))
				.signWith(SignatureAlgorithm.HS512, jwtConfig.getTokenSecret())
				.compact();
	}
	
	public Map<String,Object> validateJwtToken(String authToken) {
		try {
			Map<String,Object> body=  (Map<String, Object>) Jwts.parser().setSigningKey(jwtConfig.getTokenSecret()).parse(authToken).getBody();
			return body;
		} catch (SignatureException e) {
			throw new BearerTokenAuthException("Invalid JWT signature: " + e.getMessage());
		} catch (MalformedJwtException e) {
			throw new BearerTokenAuthException("Invalid JWT token:  " + e.getMessage()); 
		} catch (ExpiredJwtException e) {
			throw new BearerTokenAuthException("JWT token is expired:  " + e.getMessage());  
		} catch (UnsupportedJwtException e) {
			throw new BearerTokenAuthException("JWT token is unsupported:  " + e.getMessage());   
		} catch (IllegalArgumentException e) {
			throw new BearerTokenAuthException("JWT token is unsupported: " +  e.getMessage());
		}
 
	}
	
	
	
}

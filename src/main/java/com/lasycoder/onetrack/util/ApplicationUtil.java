package com.lasycoder.onetrack.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.lasycoder.onetrack.dto.ApplicationTokenDTO;

public class ApplicationUtil {
	
	public static String getCurrentUserID () {
		Authentication authenticationObject = SecurityContextHolder.getContext().getAuthentication();
		ApplicationTokenDTO parsedTokenObject = (ApplicationTokenDTO) authenticationObject.getPrincipal();
		return parsedTokenObject.getUserId();
		
	}
}

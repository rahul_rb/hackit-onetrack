/**
 * 
 */
package com.lasycoder.onetrack.util;

/**
 * @author rbarahpu
 *
 */
public class AppConstants {
	
	public static final String GRANT_TYPE_PARAM = "grant_type";
	public static final String AUTHORIZATION_CODE = "authorization_code";
	public static final String CODE_PARAM = "code";
	public static final String CLIENT_ID_PARAM = "client_id";
	public static final String CLIENT_SECRET_PARAM = "client_secret";
	public static final String REDIRECT_URI_PARAM = "redirect_uri";
	public static final int DEFAULT_PAGE_LIMIT = 500;
	public static final int DEFAULT_PAGE_OFFSET = 0;
	
}

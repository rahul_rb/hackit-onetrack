package com.lasycoder.onetrack.restcontrollers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lasycoder.onetrack.dto.ApplicationTokenDTO;
import com.lasycoder.onetrack.dto.EmployeeListResponse;
import com.lasycoder.onetrack.dto.EmployeeRequest;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.dto.SuccessResponse;
import com.lasycoder.onetrack.exceptions.ClientException;
import com.lasycoder.onetrack.model.Employee;
import com.lasycoder.onetrack.service.EmployeeService;

@RestController
@RequestMapping(path = "/api/v1")
public class EmployeeController {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	EmployeeService employeeService;
	
	@PreAuthorize("hasRole('MANAGER')")
	@RequestMapping(path = "/employee", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
	public ResponseEntity registerEmployee(@RequestBody EmployeeRequest employeeRequest) { 
		try { 
			
			String validateResponse = employeeRequest.validate();
			if (validateResponse != null) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage(validateResponse);
				return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
			}
			logger.info("Employee creation handler invoked ");
			Authentication authenticationObject = SecurityContextHolder.getContext().getAuthentication();
			ApplicationTokenDTO applicationToken = (ApplicationTokenDTO) authenticationObject.getPrincipal();
			
			if (employeeRequest.getManagerId() <= 0) {
				employeeRequest.setManagerId(Long.valueOf(applicationToken.getManagerId()));
			} 
			if (employeeRequest.getProjectId()!=null && !employeeRequest.getProjectId().equals(applicationToken.getProjectId())) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.FORBIDDEN.value()));
				error.setErrorMessage(validateResponse);
				return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
			} 
			
			
			employeeRequest.setProjectId(applicationToken.getProjectId());
			
			logger.info("Project Id :: " + employeeRequest.getProjectId());
			
			long employeeId = employeeService.addEmployee(employeeRequest, applicationToken.getUserId());
			
			SuccessResponse success = new SuccessResponse();
			success.setEmployeeId(String.valueOf(employeeId));
			logger.info("Employee created successfully.");
			return new ResponseEntity(success,HttpStatus.OK);
		} catch (Exception ex) {
			logger.error("Exception at registerEmployee due to following reason ", ex);
			ErrorModel error = new ErrorModel();
			if (ex instanceof ClientException) {
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage(ex.getMessage());
			} else {
				error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
				error.setErrorMessage("Internal Server Error");
			}
			return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
		} 
	}
	
	@RequestMapping(path = "/employee/{employeeId}", method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity fetchEmployeeDetailsById(@PathVariable("employeeId") Long employeeId) {
		try {
			Optional<Employee> employeeData = employeeService.getEmployeeById(employeeId);
			
			if (!employeeData.isPresent()) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage("Employee details not exits");
				return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode()))); 
			}
			return new ResponseEntity(employeeData.get(),HttpStatus.OK);
		} catch(Exception ex) {
			logger.error("Exception at fetchEmployeeDetailsById due to following reason ", ex);
			ErrorModel error = new ErrorModel();
			error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			error.setErrorMessage(ex.getMessage());
			
			return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode()))); 
		}
	}
	
 //	/employees?filterType=project|manager|initiative&query=projectId=Rialto|employeeRole=MANAGER&limit&offset
	
	@RequestMapping(path = "/employees", method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<EmployeeListResponse> fetchEmployees(
			@RequestParam(value="filterType",defaultValue = "project") String filterType,
			@RequestParam(value="queryString",defaultValue = "") String queryString,
			@RequestParam(value="limit",defaultValue = "500") int limit,
			@RequestParam(value="offset",defaultValue = "0") int offset) {
		
		try {
			Map<String,Object> queryMap = parseFilterQuery(queryString);
			EmployeeListResponse employeeListResponse = new EmployeeListResponse();
			if (queryMap.size()==0) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage("Filter Query is invalid");  
				employeeListResponse.setError(error);
				employeeListResponse.setResponseCode(HttpStatus.BAD_REQUEST);
				
				return new ResponseEntity<EmployeeListResponse>(employeeListResponse, employeeListResponse.getResponseCode()); 
			}
			
			employeeListResponse = employeeService.fetchEmployeesBasedOnDynamicQuery(filterType, queryMap, limit, offset); 
			return new ResponseEntity<EmployeeListResponse>(employeeListResponse,employeeListResponse.getResponseCode());
		} catch (Exception ex) {
			logger.error("Exception at fetchEmployees due to following reason ", ex);
			EmployeeListResponse employeeListResponse = new EmployeeListResponse();
			
			if (ex instanceof ClientException) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage(ex.getMessage()); 
				employeeListResponse.setError(error);
				employeeListResponse.setResponseCode(HttpStatus.BAD_REQUEST); 
			} else {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
				error.setErrorMessage(ex.getMessage()); 
				employeeListResponse.setError(error);
				employeeListResponse.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR); 
			} 
			return new ResponseEntity<EmployeeListResponse>(employeeListResponse, employeeListResponse.getResponseCode()); 
		}
	}
	
	
	private Map<String,Object> parseFilterQuery(String queryString) throws Exception {
		List<String> queryArray = Arrays.asList(queryString.split("\\,"));
		Map<String,Object> queryMap = new HashMap<>(); 
		
		queryArray.stream().map(str -> {
			String[] qA = str.split("\\=");
			if(qA.length==2) {
				String arr[] = new String[] {qA[0],qA[1]};
                                
				return arr;
			}
			return null;
		}).forEach(arr -> { 
			if (arr!=null) {
				 queryMap.put(arr[0], arr[1]); 
			}    
		});
		
		return queryMap;
		
	}
}

/**
 * 
 */
package com.lasycoder.onetrack.restcontrollers;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.dto.InitiativeListResponse;
import com.lasycoder.onetrack.dto.InitiativeRequest;
import com.lasycoder.onetrack.dto.InitiativeResponse;
import com.lasycoder.onetrack.service.InitiativeService;
import com.lasycoder.onetrack.util.AppConstants;

/**
 * @author rbarahpu
 *
 */

@RestController
@RequestMapping(path = "/api/v1")
public class InitiativeController {

	@Autowired
	private InitiativeService initiativeService;
	
	@PreAuthorize("hasRole('MANAGER')")
	@RequestMapping(path = "initiative", method = RequestMethod.POST)
	public ResponseEntity<InitiativeResponse> addInitiative(@Valid @RequestBody final InitiativeRequest requestBody) { 
		InitiativeResponse response = new InitiativeResponse();
		try {
			response = initiativeService.addInitiative(requestBody);
			return new ResponseEntity<InitiativeResponse>(response, response.getResponseCode());

		} catch (Exception e) {

			ErrorModel errorModel = frameISEErrorModel(e);
			response.setErrorModel(errorModel);
			return new ResponseEntity<InitiativeResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(path = "initiatives", method = RequestMethod.PUT)
	public ResponseEntity<InitiativeResponse> updateInitiative(@RequestBody final InitiativeRequest requestBody) {
		InitiativeResponse response = new InitiativeResponse();
		try {
			InitiativeResponse bodyResponse = initiativeService.updateInitiative(requestBody);
			return new ResponseEntity<InitiativeResponse>(bodyResponse, bodyResponse.getResponseCode());
		} catch (Exception e) {

			ErrorModel errorModel = frameISEErrorModel(e);
			response.setErrorModel(errorModel);
			response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
			return new ResponseEntity<InitiativeResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	@RequestMapping(path = "initiatives", method = RequestMethod.GET)
	public ResponseEntity<InitiativeListResponse> getAllInitiative(
			@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset) {

		InitiativeListResponse response = new InitiativeListResponse();
		try {
			if (limit == null) {
				limit = AppConstants.DEFAULT_PAGE_LIMIT;
			}
			if (offset == null) {
				offset = AppConstants.DEFAULT_PAGE_OFFSET;
			}
			response = initiativeService.getAllInitiatives(limit, offset);
			return new ResponseEntity<InitiativeListResponse>(response, response.getResponseCode());
		} catch (Exception e) {
			ErrorModel errorModel = frameISEErrorModel(e);
			response.setErrorModel(errorModel);
			return new ResponseEntity<InitiativeListResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ErrorModel frameISEErrorModel(Exception e) {
		ErrorModel errorModel = new ErrorModel();
		errorModel.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		errorModel.setErrorMessage(
				"Exception occurred while accessing initiative. Please contact administrator: " + e.getMessage());
		return errorModel;
	}

	@RequestMapping(path = "initiative/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<InitiativeResponse> deleteInitiativeById(@PathVariable(name = "id") Long id) {
		
		InitiativeResponse response = new InitiativeResponse();
		
		try {
			
			response = initiativeService.deleteInitiativeById(id);
			return new ResponseEntity<InitiativeResponse>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			
			ErrorModel errorModel = frameISEErrorModel(e);
			response.setErrorModel(errorModel);
			return new ResponseEntity<InitiativeResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(path = "/project/{projectId}/initiatives")
	public ResponseEntity<InitiativeListResponse> getInitiativesByProjectId(@PathVariable(name = "projectId") final String projectId, @RequestParam(name = "limit", required = false) Integer limit, @RequestParam(name = "offset", required = false) Integer offset) {
		
		if (limit == null) {
			limit = AppConstants.DEFAULT_PAGE_LIMIT;
		}
		if (offset == null) {
			offset = AppConstants.DEFAULT_PAGE_OFFSET;
		}
		InitiativeListResponse response = new InitiativeListResponse();
		try {
			response = initiativeService.getInitiativesByProjectId(projectId, limit, offset);
			return new ResponseEntity<InitiativeListResponse>(response, response.getResponseCode());
			
		} catch (Exception e) {
			ErrorModel errorModel = frameISEErrorModel(e);
			response.setErrorModel(errorModel);
			return new ResponseEntity<InitiativeListResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.lasycoder.onetrack.restcontrollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lasycoder.onetrack.dto.ApplicationTokenDTO;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.exceptions.OAuth2Exception;
import com.lasycoder.onetrack.exceptions.UserNotFoundException;
import com.lasycoder.onetrack.service.AuthenticationService;

@RestController
@RequestMapping(path = "/api/auth")
public class CiscoCIAuthenticationController {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	AuthenticationService authenticationService;

	@RequestMapping(path = "/redirect/cisco", method = RequestMethod.GET)
	public ResponseEntity authenticateCI(@RequestParam("userId") String userId,@RequestParam(value="redirect",defaultValue = "false") boolean redirect) {
		try {
			if (userId == null || userId.trim().length() < 2) {
				ErrorModel error = new ErrorModel();
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage("Please Provide userid");
				return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
			}
			
			String urlToRedirectOAuth2 = authenticationService.generateOAuth2RedrectUrl(userId);
			if (redirect) {
				return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header("Location", urlToRedirectOAuth2).build();
			} else {
				String redirectJson = "{\"redirect_url\": \"" +urlToRedirectOAuth2+ "\"}";
				return new ResponseEntity<>(redirectJson, HttpStatus.OK);
			}
			
		} catch (Exception ex) {
			logger.error("Exception at authenticateCI due to following reason ", ex);
			ErrorModel error = new ErrorModel();
			if (ex instanceof UserNotFoundException) {
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage(ex.getMessage());
			} else {
				error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
				error.setErrorMessage("Internal Server Error");
			}
			return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
		} 
	}

	@RequestMapping(path = "/callback/cisco", method = RequestMethod.GET)
	public ResponseEntity handleCiscoCI(@RequestParam("code") String code, @RequestParam("state") String state) {
		if (code == null || state == null || code.trim().length() < 5 || state.trim().length() < 5) {
			ErrorModel error = new ErrorModel();
			error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
			error.setErrorMessage("Provide code and state");
			return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
		}

		logger.info("Getting Called by Token Provider to Authenticate User");

		try {
			ApplicationTokenDTO applicationDTO = authenticationService.generateApplicationAccessToken(code, state);
			logger.info("User got authenticated successfully");
			return new ResponseEntity<>(applicationDTO, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error("Exception at handleCiscoCI due to following reason ", ex);
			ErrorModel error = new ErrorModel();
			if (ex instanceof OAuth2Exception) {
				error.setErrorCode(String.valueOf(HttpStatus.UNAUTHORIZED.value()));
				error.setErrorMessage(ex.getMessage());
			} else if (ex instanceof UserNotFoundException) {
				error.setErrorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
				error.setErrorMessage(ex.getMessage());
			} else {
				error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
				error.setErrorMessage("Internal Server Error");
			}
			return new ResponseEntity<>(error, HttpStatus.valueOf(Integer.valueOf(error.getErrorCode())));
		}
	}
}

package com.lasycoder.onetrack.restcontrollers;

import java.util.Date;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lasycoder.onetrack.dto.AllocationListResponse;
import com.lasycoder.onetrack.dto.AllocationRequest;
import com.lasycoder.onetrack.dto.AllocationResponse;
import com.lasycoder.onetrack.dto.ApplicationTokenDTO;
import com.lasycoder.onetrack.dto.EmployeeListResponse;
import com.lasycoder.onetrack.dto.ErrorModel;
import com.lasycoder.onetrack.service.AllocationService;
import com.lasycoder.onetrack.util.AppConstants;

@RestController
@RequestMapping(path = "/api/v1")
public class AllocationController {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	AllocationService allocationService;

	@RequestMapping(path = "/allocation", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<AllocationResponse> allocateEmploteeToInitiative(
			@Valid @RequestBody AllocationRequest allocation) {
		try {
			Authentication authenticationObject = SecurityContextHolder.getContext().getAuthentication();
			ApplicationTokenDTO applicationToken = (ApplicationTokenDTO) authenticationObject.getPrincipal();

			AllocationResponse allocationRespose = allocationService.createAllocationByProject(
					applicationToken.getProjectId(), allocation, applicationToken.getUserId());

			return new ResponseEntity<AllocationResponse>(allocationRespose, allocationRespose.getResponseCode());
		} catch (Exception ex) {
			logger.error("Exception in allocateEmploteeToInitiative due to following reason  ", ex);
			AllocationResponse response = new AllocationResponse();
			ErrorModel error = new ErrorModel();
			error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			error.setErrorMessage(ex.getMessage());
			response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setErrorModel(error);

			return new ResponseEntity<AllocationResponse>(response, response.getResponseCode());
		}
	}

	@RequestMapping(path = "/allocation/employees/available", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<EmployeeListResponse> fetchAvailableEmployeesForAllocation(
			@RequestParam("dateStamp") long dateStamp) {
		try {
			Authentication authenticationObject = SecurityContextHolder.getContext().getAuthentication();
			ApplicationTokenDTO applicationToken = (ApplicationTokenDTO) authenticationObject.getPrincipal();

			EmployeeListResponse allocationRespose = allocationService
					.getAvailableEmployeesOnGivenTimePerProject(applicationToken.getProjectId(), dateStamp);

			return new ResponseEntity<EmployeeListResponse>(allocationRespose, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error("Exception in fetchAvailableEmployeesForAllocation due to following reason  ", ex);
			EmployeeListResponse response = new EmployeeListResponse();
			ErrorModel error = new ErrorModel();
			error.setErrorCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
			error.setErrorMessage(ex.getMessage());
			response.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setError(error);

			return new ResponseEntity<EmployeeListResponse>(response, response.getResponseCode());
		}

	}

	// /allocations?st=78278687628&et=1774947573&userId=absc@cisco.com
	
	@GetMapping(value = "/allocations")
	public ResponseEntity<AllocationListResponse> getAllocationsByTimeRangeEmployeeId(@RequestParam (name = "st") @DateTimeFormat(pattern = "MMddyyyy") Date startTime, @RequestParam(name = "et") @DateTimeFormat(pattern = "MMddyyyy") Date endTime,
			@RequestParam(name = "userId", required = false) String userId, @RequestParam(name = "limit", required = false)Integer limit, @RequestParam(name = "offset", required = false) Integer offset) {

		if (limit == null) {
			limit = AppConstants.DEFAULT_PAGE_LIMIT;
		}
		if (offset == null) {
			offset = AppConstants.DEFAULT_PAGE_OFFSET;
		}

		AllocationListResponse allocationListResponse = new AllocationListResponse();
		try {
			allocationListResponse = allocationService.getAllocationsByTimeRangeEmployeeId(startTime, endTime, userId,
					offset, limit);
			return new ResponseEntity<AllocationListResponse>(allocationListResponse, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Some error has occurred. Please contact administrator",e);
			ErrorModel errorModel = new ErrorModel(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					"Some error has occurred. Please contact administrator: " + e.getMessage());
			allocationListResponse.setError(errorModel);
			return new ResponseEntity<AllocationListResponse>(allocationListResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}

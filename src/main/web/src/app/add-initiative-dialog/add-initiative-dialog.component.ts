import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InitiativeService } from '../initiative/initiative-service';

@Component({
  selector: 'app-add-initiative-dialog',
  templateUrl: './add-initiative-dialog.component.html',
  styleUrls: ['./add-initiative-dialog.component.css']
})
export class AddInitiativeDialogComponent implements OnInit {
  addInitiativeError: string;
  initId: string;
  initDesc: string;

  isEditMode: boolean = false;

  statusList= ["INPROGRESS","COMPLETED"];
  selectedStatus: string;

  constructor(private dialogRef: MatDialogRef<AddInitiativeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private initiativeService: InitiativeService) {
    if (data["isEditMode"]) {
      this.isEditMode = data.isEditMode;
    }

    if (data["initiativeData"]) {
      this.initId = data["initiativeData"]["initiativeId"];
      this.initDesc = data["initiativeData"]["initiativeDesc"];
    }
  }

  ngOnInit(): void {
  }

  onSaveClick() {
    let iniativeObj = {
      "initiativeDesc": this.initDesc,
      "initiativeId": this.initId,
      "status":this.selectedStatus
    }
    if (!this.isEditMode) {
      this.initiativeService.addInitiativeToProject(iniativeObj)
        .subscribe((response) => {
          this.dialogRef.close({ change: true })
        }, (err) => {
          this.addInitiativeError = err["error"]["errorMessage"];
        });

    } else {
      this.initiativeService.updateInitiative(iniativeObj)
      .subscribe((response) => {
        this.dialogRef.close({ change: true })
      }, (err) => {
        this.addInitiativeError = err["error"]["errorMessage"];
      });
    }

  }
}

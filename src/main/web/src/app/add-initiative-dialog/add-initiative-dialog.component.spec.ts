import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInitiativeDialogComponent } from './add-initiative-dialog.component';

describe('AddInitiativeDialogComponent', () => {
  let component: AddInitiativeDialogComponent;
  let fixture: ComponentFixture<AddInitiativeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInitiativeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInitiativeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

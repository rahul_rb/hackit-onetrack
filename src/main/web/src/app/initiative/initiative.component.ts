import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddInitiativeDialogComponent } from '../add-initiative-dialog/add-initiative-dialog.component';
import { InitiativeService } from './initiative-service';

export class InitiativeRow {
  id: string
  initiativeId: string;
  initiativeDesc: string;
  status: string

  public InitiativeRow(id: string,inId: string, iniDesc: string, status: string) {
    this.id = id
    this.initiativeId = inId;
    this.initiativeDesc = iniDesc;
    this.status = status
  }
}

@Component({
  selector: 'app-initiative',
  templateUrl: './initiative.component.html',
  styleUrls: ['./initiative.component.css']
})
export class InitiativeComponent implements OnInit {


  displayedColumns: string[] = ['initiativeId', 'initiativeDesc', 'status','actions'];
  dataSource = []; 

  constructor(private initiativeService: InitiativeService,private addInitativeDialog: MatDialog) { }

  ngOnInit(): void {
    this.fetchInitiativeListPerProject();
  }

  fetchInitiativeListPerProject() {
    this.dataSource = [];
    this.initiativeService.fetchInitiativeBasedOnProjectId()
      .subscribe(response => {
        let initiativeListObj = response["initiatives"];
        let resultList = initiativeListObj["results"];
        let initiativeList: InitiativeRow[] = [];

        for (let i in resultList) {
          let element = resultList[i];
          var ini = new InitiativeRow();
          ini.id = element["initiativeId"];
          ini.initiativeId = element["initiativeStringId"];
          ini.initiativeDesc = element["initiativeDesc"];
          ini.status = element["status"]

          initiativeList.push(ini);
        }
        this.dataSource = initiativeList;
      }, (err) => {
        this.dataSource = [];
      });
  }

  onAddInitiativeClick() {
    let dialogRef = this.addInitativeDialog.open(AddInitiativeDialogComponent, {
      disableClose: true,
      width: '800px',
      data: {
          
      }
    }).afterClosed().subscribe(function (changedContent) {
      if (changedContent) {
        this.fetchInitiativeListPerProject();  
        
      }
    }.bind(this));
  }

  openEditInitiativeDialog(initElem) {
    let dialogRef = this.addInitativeDialog.open(AddInitiativeDialogComponent, {
      disableClose: true,
      width: '800px',
      data: {
          isEditMode: true,
          initiativeData: initElem
      }
    }).afterClosed().subscribe(function (changedContent) {
      if (changedContent) {
        this.fetchInitiativeListPerProject();  
        
      }
    }.bind(this));
  }

  deleteInitiative(initEleme) {
    this.initiativeService.deleteInitative(initEleme.id)
      .subscribe(response => {
        this.fetchInitiativeListPerProject();  
      },(error) => {
        console.log(error);
      })
      
  }
}

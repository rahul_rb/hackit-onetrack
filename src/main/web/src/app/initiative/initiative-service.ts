import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApplicaionUtil } from '../ApplicationUtil';
import { HttpClientCommon } from '../common/httpclient-common.service';
import { SystemInfoService } from '../common/systeminfo.service';

@Injectable()
export class InitiativeService {

  private url = SystemInfoService.getAPIHost() + "/api/v1";
  constructor(private httpService: HttpClientCommon) { }


  fetchInitiativeBasedOnProjectId() {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId = ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");
      let path = "/project/" + projectId + "/initiatives";

      let fullUrl = this.url + path;

      return this.httpService.fireGetRequest(fullUrl);
    }
  }

  addInitiativeToProject(initiativeContent) {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId = ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");
      initiativeContent["projectId"] = projectId;
      let path = "/initiative";

      let fullUrl = this.url + path;

      return this.httpService.firePostRequest(fullUrl, initiativeContent);
    }
  }

  updateInitiative(initiativeContent) {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId = ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");
      initiativeContent["projectId"] = projectId;
      let path = "/initiatives";

      let fullUrl = this.url + path;

      return this.httpService.firePutRequest(fullUrl, initiativeContent);
    }
  }

  deleteInitative(initiativeId) {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let path = "/initiative/" + initiativeId;

      let fullUrl = this.url + path;

      return this.httpService.fireDeleteRequest(fullUrl);
    }
  } 
}

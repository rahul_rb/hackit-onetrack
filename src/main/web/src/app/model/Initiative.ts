export class Initiative {

    private id;
    private initiativeId;
    private initiativeDesc;
    private startTime;
    private endTime;
    private status;

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }
    getInitiativeId() {
        return this.initiativeId;
    }

    setInitiativeId(initiativeId) {
        this.initiativeId = initiativeId;
    }

    getStartTime() {
        return this.startTime;
    }

    setStartTime() {
        this.startTime = this.startTime;
    }

    getEndTime() {
        return this.endTime;
    }

    setEndTime(endTime) {
        this.endTime = endTime;
    }

    getInitiativeDesc() {
        return this.initiativeDesc;
    }

    setInitiativeDesc(initiativeDesc) {
        this.initiativeDesc = initiativeDesc;
    } 

    getStatus() {
        return this.status;
    }

    setStatus(status) {
        this.status = status;
    }
    
}
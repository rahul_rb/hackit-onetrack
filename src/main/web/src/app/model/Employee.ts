export class Employee {
    private name;
    private id;
    private userId;
    private emailId;
    private managerId;
    private role;
    private managerName;

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getUserId() {
        return this.userId;
    }

    setUserId(userId) {
        this.userId = userId;
    }

    getEmailId() {
        return this.emailId;
    }
    
    setEmailId(emailId) {
        this.emailId = emailId;
    }

    getManagerId() {
        return this.managerId;
    }

    setManagerId(managerId) {
        this.managerId = managerId;
    } 

    getRole() {
        return this.role;
    }

    setRole(role) {
        this.role = role;
    }

    getManagerName() {
        return this.managerName;
    }

    setManagerName(managerName) {
        this.managerName = managerName;
    }
}
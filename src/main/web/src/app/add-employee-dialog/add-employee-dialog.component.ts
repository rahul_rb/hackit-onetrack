import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EmployeeService } from '../employee/employee-service';

@Component({
  selector: 'app-add-employee-dialog',
  templateUrl: './add-employee-dialog.component.html',
  styleUrls: ['./add-employee-dialog.component.css']
})
export class AddEmployeeDialogComponent implements OnInit {
  addEmployeeError: string= "";
  userId: string;
  empName: string;
  employeeRoles: string = "";
  rolesList = [{id:"EMPLOYEE",value:"Employee"},
                {id:"ARCHITECT",value:"Architect"},
                {id:"MANAGER",value:"Manager"}];
  managerList = [];
  selectedManger = "";

  constructor( private dialogRef: MatDialogRef<AddEmployeeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private employeeService: EmployeeService) { 
      console.log(data.managerList)
      this.managerList = data.managerList
    } 

  ngOnInit(): void {
  }

  onSaveClick() {
    console.log(this.userId);
    console.log(this.empName);
    console.log(this.employeeRoles);
    console.log(this.selectedManger);

    let employeeObject = {
      "emailAddress": this.userId,
      "employeeFullName": this.empName,
      "employeeRole": this.employeeRoles,
      "managerId": this.selectedManger, 
    }
    this.employeeService.addEmployeeDetails(employeeObject)
      .subscribe((response) => {
        this.dialogRef.close({ change: true })
      },(errorObj: Error) =>  { 
        this.addEmployeeError = errorObj["error"]["errorMessage"];
      });
  }

}



export class ApplicaionUtil {

    public static getAccessToken() :string {
        try {
            let accessTokenObj = JSON.parse(localStorage.getItem("accessToken"));
            return accessTokenObj["accessToken"];
        } catch(ex) {
            return null;
        }
    }

    public static getDetailsFromAccessTokenObject(key: string) :string {
        try {
            let accessTokenObj = JSON.parse(localStorage.getItem("accessToken"));
            return accessTokenObj[key];
        } catch(ex) {
            return null;
        }
    }
 
}
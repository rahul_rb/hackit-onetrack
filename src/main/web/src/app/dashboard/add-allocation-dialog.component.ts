import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CacheService } from '../common/cache.service';
import { EmployeeService } from '../employee/employee-service';
import { DashboardService  } from './dashboard.service'; 

@Component({
    selector: 'add-allocation-dialog.component.ts',
    templateUrl: 'add-allocation-dialog.component.html'
  })

  export class AddAllocationDialogComponent {

    addAllocationError: string;
    userId: string;
    initId: string;
    startTime: string;
    endTime: string;
    employeeList = new Array();
    selectedEmployee;

    constructor(private dialogRef: MatDialogRef<AddAllocationDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,private dashboardService: DashboardService, private employeeService: EmployeeService, private cache: CacheService) {

        
      }

      onSaveClick() {

        let self = this;
        let addAllocationRequestBody: AddAllocationRequest = {
          employeeUserId: this.selectedEmployee.employeeId,
          initiativeId: this.initId,
          startTime: new Date(this.startTime).getTime(),
          endTime: new Date(this.endTime).getTime()

        }
        // let addAllocationRequestBody2: AddAllocationRequest;
        // this.employeeService.getEmployeeById(this.selectedEmployee.managerId)
        // .subscribe((res) => {

        //    addAllocationRequestBody2 = {
        //     employeeUserId: res['userId'],
        //     initiativeId: self.initId,
        //     startTime: addAllocationRequestBody.startTime,
        //     endTime: addAllocationRequestBody.endTime
        //   }
        //   self.dashboardService.addAllocation(addAllocationRequestBody2, this.addMngrAllocationCB.bind(this));

        // }, (err) => {
        //   this.addAllocationError = err["error"]["errorMessage"];
        // });
        
        this.dashboardService.addAllocation(addAllocationRequestBody, this.addAllocationCB.bind(this));
        
        
      }

      addMngrAllocationCB(res) {
        if (res['error']) {
          this.addAllocationError = res["error"]["errorMessage"];
        }
      }

      addAllocationCB(res) {

        if (!res["error"]) {
          this.dialogRef.close({ change: true })
        } else {
          this.addAllocationError = res["error"]["errorMessage"];
        }
      }

      getAvailableEmployees() {

        this.employeeService.getAvailableEmployees(this.getAvailableEmployeesCB.bind(this));
    
      }

      getAvailableEmployeesCB (res) {

        let self = this;
        self.employeeList = [];
        if (res.employees) {
          res.employees.results.forEach(element => {
            self.employeeList.push({
              fullName: element.fullName,
              employeeId: element.employeeId,
              managerId: element.managerId
            })
          });
        }
      }
  }

  

export interface AddAllocationRequest {
  employeeUserId;
  initiativeId;
  startTime;
  endTime
}

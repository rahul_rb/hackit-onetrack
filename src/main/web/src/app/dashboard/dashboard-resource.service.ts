import { Injectable } from "@angular/core";
import { SystemInfoService } from "../common/systeminfo.service";

@Injectable()
export class DashboardResourceService {

    serverUrl = 'http://localhost:6060';

    constructor(private systemInfo: SystemInfoService) {

    }

    RESOURCES = {
        ALLOCATIONS_PATH: 'allocations',
        ALLOCATION_PATH: 'allocation',
        START_TIME_PARAM: 'st',
        END_TIME_PARAM: 'et'
    }


    frameURLGetAllocationsByTimeRange (data) {

        let url;
        url = `${SystemInfoService.getAPIHost()}/api/v1/${this.RESOURCES.ALLOCATIONS_PATH}?${this.RESOURCES.START_TIME_PARAM}=${data.startTimeInMMDDYYY}&${this.RESOURCES.END_TIME_PARAM}=${data.endTimeInMMDDYYYY}`;

        return url;
    }

    frameURLGetAllocationsByTimeRangeAndEmployee (data) {

        let url = this.frameURLGetAllocationsByTimeRange(data);
        url += `&userId=${data.userId}`;
        return url;
    }

    frameURLAddAllocation() {
        let url = `${SystemInfoService.getAPIHost()}/api/v1/${this.RESOURCES.ALLOCATION_PATH}`;
        return url;
    }

}
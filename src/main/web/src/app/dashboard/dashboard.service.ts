import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HttpClientCommon } from "../common/httpclient-common.service";
import { DashboardResourceService } from "./dashboard-resource.service";

@Injectable()
export class DashboardService {

    constructor(private dashboardResource: DashboardResourceService, private httpService: HttpClientCommon) { }

    getAllocationsByTimeRange(data, callback) {

        let url = this.dashboardResource.frameURLGetAllocationsByTimeRange(data);
    
        this.httpService.fireGetRequest(url)
        .subscribe((res) => {
            callback(res);
        }, (err) => {
            callback(err);
        })
    }

    addAllocation(addAllocationRequestBody, callback) {

        let url = this.dashboardResource.frameURLAddAllocation();
        this.httpService.firePostRequest(url, addAllocationRequestBody)
        .subscribe((res) => {
            callback(res);
        }, (err) => {
            callback(err);
        })


    }
}
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicaionUtil } from '../ApplicationUtil';
import { DashboardComponent } from './dashboard.component';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['dashboard.component.css'],
})

export class HeaderComponent {

  active: string = "dashboard";
  titleShow: string = "Dashboard"
  constructor(private router: Router) {
    let accessTokenObj = ApplicaionUtil.getAccessToken();
    if (!accessTokenObj) {
      this.router.navigate(["/"]);
    }
  }

  onLogout() {
    localStorage.removeItem("accessToken");
    this.router.navigate(["/"]);
  }
}
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { UtilityService } from '../common/util.service';
import { DashboardService } from './dashboard.service';
import { CacheService } from '../common/cache.service';
import { MatDialog } from '@angular/material/dialog';
import { AddAllocationDialogComponent } from './add-allocation-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DashboardComponent implements OnInit {
  
  emailId: string;
  ELEMENT_DATA: ManagerAllocation[] = new Array();
  dataSource;
  childDataSource;
  ALLOCATIONS_DATA: Allocation[] = new Array();
  parentColumns: string[] = ['ManagerName', 'Initiatives', 'position'];
  childColumns: string[] = ['FullName', 'Initiative', 'StartTime', 'EndTime'];
  
  expandedElement: ManagerAllocation | null;
  

  constructor(private router: Router, private util: UtilityService, private dashboardService: DashboardService, private cache: CacheService, private addAllocationDialog: MatDialog) { 
    
  }

  ngOnInit(): void {
    try {
      let accessTokenObj = JSON.parse(localStorage.getItem("accessToken"));
      if (accessTokenObj != null && accessTokenObj["accessToken"] && accessTokenObj["role"]) {
        console.log("success login");
        this.emailId = accessTokenObj["emailId"];
        this.populateDashboard();

      } else {
        this.router.navigate(["/login"]);
      }
    } catch (ex) {
      this.router.navigate(["/login"]);
    }
  }

  populateDashboard() {

    let startTime = this.util.getStartTime(15);
    let endTime = this.util.getEndTime(15);
    let startTimeInMMDDYYY = this.util.convertDateToMMDDYYYFormat(startTime);
    let endTimeInMMDDYYYY = this.util.convertDateToMMDDYYYFormat(endTime);
    
    this.dashboardService.getAllocationsByTimeRange({startTimeInMMDDYYY, endTimeInMMDDYYYY}, this.getAllocationsByTimeRangeCB.bind(this));

  }

  populateEmployeeTable(data) {

    let self = this;
    console.log(data);
    let initiativesEmployeeMap = this.cache.getInitiativesEmployeeMapping();
    this.ALLOCATIONS_DATA = [];
    initiativesEmployeeMap.forEach(element => {
      if (element.employeeResponse.managerUserId && element.employeeResponse.managerUserId === data.ManagerId && element.employeeResponse.employeeRole === 'EMPLOYEE') {

        console.log('START TIME - ', element.initiativeResponseList[0].startTime);
        this.ALLOCATIONS_DATA.push({
          FullName: element.employeeResponse.fullName,
          Initiative: element.initiativeResponseList[0].initiativeId,
          StartTime: element.initiativeResponseList[0].startTime,
          EndTime: element.initiativeResponseList[0].endTime
        })
      }
    });
    this.childDataSource = this.ALLOCATIONS_DATA;
    
  }

  getAllocationsByTimeRangeCB (res) {

    let self = this;
    if (res.initiativesEmployeeMappings) {
      console.log('RESPONSE -> ', res)
      let response = res.initiativesEmployeeMappings;
      this.cache.setInitiativesEmployeeMapping(response);
      for (let i = 0; i < response.length; i++) {
        let p = 1;
        if (response[i].employeeResponse.employeeRole && response[i].employeeResponse.employeeRole === 'MANAGER') {
          let initiatives = [];
          for (let i = 0; i < response[i].initiativeResponseList.length; i++) {
            initiatives.push(response[i].initiativeResponseList[i]);
          }
          this.ELEMENT_DATA.push({
            ManagerName: response[i].employeeResponse.fullName,
            Initiatives: initiatives.join(),
            position: p,
            ManagerId: response[i].employeeResponse.employeeId
          });
          p++;
        }
      }

      this.dataSource = this.ELEMENT_DATA;

    }
  }

  addAllocation () {

    let self = this;
    let dialogRef = this.addAllocationDialog.open(AddAllocationDialogComponent, {
      disableClose: true,
      width: '800px',
      
    }).afterClosed().subscribe(function (changedContent) {
      if (changedContent) {
        self.populateDashboard(); 
        
      }
    }.bind(this));
  
  }

  
}



export interface ManagerAllocation {
  ManagerName: string;
  position: number;
  Initiatives: string;
  ManagerId: number;
}

export interface Allocation {
  FullName: string;
  Initiative: string;
  StartTime: string;
  EndTime:string;
}



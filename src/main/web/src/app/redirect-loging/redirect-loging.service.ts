import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SystemInfoService } from '../common/systeminfo.service';

@Injectable()
export class RedirectLogingService {
  
  private tokenVerifyUriPath = SystemInfoService.getAPIHost() + "/api/auth/callback/cisco?code=<code>&state=<state>";
  constructor(private http: HttpClient) { }
 

  authenticateCIWithAuthorizationCode(code: string, state: string) {
    this.tokenVerifyUriPath = this.tokenVerifyUriPath.replace("<code>",code).replace("<state>",state);
    return this.http.get(this.tokenVerifyUriPath,{
      headers: new HttpHeaders({
        'Content-Type':  'application/json;charset=UTF-8',
        'Accept':  'application/json;charset=UTF-8' 
      })
    })
  }

}

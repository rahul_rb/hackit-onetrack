import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {RedirectLogingService} from './redirect-loging.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect-loging',
  templateUrl: './redirect-loging.component.html',
  styleUrls: ['./redirect-loging.component.css']
})
export class RedirectLogingComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute,private redirectService: RedirectLogingService) { }

  private code: string;
  private state: string; 


  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params.code && params.state) {
          this.code = params.code;
          this.state = params.state;

          this.redirectService.authenticateCIWithAuthorizationCode(this.code,this.state)
            .subscribe((response: Response) => { 
              localStorage.setItem("accessToken",JSON.stringify(response));
              this.router.navigate(["/dashboard"]);
            },(errorObj: Error) => {
              console.log(errorObj["error"]);
              this.router.navigate(["/"]);
            }); 
        } else {
          this.router.navigate(["/"]);
        } 
      }
    );
  }

}

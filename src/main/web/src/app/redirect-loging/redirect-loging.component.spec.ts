import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectLogingComponent } from './redirect-loging.component';

describe('RedirectLogingComponent', () => {
  let component: RedirectLogingComponent;
  let fixture: ComponentFixture<RedirectLogingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectLogingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectLogingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

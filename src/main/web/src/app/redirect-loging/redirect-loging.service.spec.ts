import { TestBed } from '@angular/core/testing';

import { RedirectLogingService } from './redirect-loging.service';

describe('RedirectLogingService', () => {
  let service: RedirectLogingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RedirectLogingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

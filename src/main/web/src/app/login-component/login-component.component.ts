import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from './login-service.service';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
  emailAddress: string;
  isEmailNotExists: boolean = false;

  constructor(private route: ActivatedRoute, private loginService: LoginServiceService, @Inject(DOCUMENT) private document: Document, private router: Router) {

  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params.code && params.state) {
          this.router.navigate(["/redirect/dashboard"], {
            queryParams: params,
            // skipLocationChange: true
          });
        } else {
          this.router.navigate(["/"])
        }
      }
      );


  }

  onEnterEmailAddress(emailId) {
    this.emailAddress = emailId;
    console.log(this.emailAddress);
  }

  onCLickLoginCI(emailId) {
    this.emailAddress = emailId;
    this.loginService.fetchCIURL(this.emailAddress).subscribe((response: Response) => {
      var responseobject = response;
      console.log(responseobject);
      this.isEmailNotExists = false;

      // Redirect to CI
      this.document.location.href = responseobject["redirect_url"];

    }, (errorObj: Error) => {
      let errorContent = errorObj["error"];
      this.isEmailNotExists = true;
      console.log(errorObj)
    });
  }

}

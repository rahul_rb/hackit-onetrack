import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SystemInfoService } from '../common/systeminfo.service';

@Injectable()
export class LoginServiceService {
  private url = SystemInfoService.getAPIHost() +  "/api/auth/redirect/cisco?userId="; 
  constructor(private http: HttpClient) { }

  fetchCIURL(emailId: string) {
    return this.http.get(this.url + emailId,{
      headers: new HttpHeaders({
        'Content-Type':  'application/json;charset=UTF-8',
        'Accept':  'application/json;charset=UTF-8' 
      })
    })
  } 
}

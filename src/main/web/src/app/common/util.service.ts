import { Injectable } from "@angular/core";

@Injectable()
export class UtilityService {

    convertDateToMMDDYYYFormat (date: Date) {

        let d = date.getDate();
        let m = date.getMonth()+1;
        let yyyy = date.getFullYear();

        let dd = d / 10 >= 1 ? d : `0${d}`;

        let mm = m / 10 >= 1 ? m : `0${m}`;

        return `${mm}${dd}${yyyy}`;
    }

    getStartTime(diff = 0) {

        let now = new Date();
        let startTimestamp = now.setDate(now.getDate() - diff);
        return new Date(startTimestamp);
    }

    getEndTime(diff = 0) {
        let now = new Date();
        let endTimestamp = now.setDate(now.getDate() + diff);
        return new Date(endTimestamp);
    }

}
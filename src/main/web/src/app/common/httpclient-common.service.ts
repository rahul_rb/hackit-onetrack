import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { ApplicaionUtil } from '../ApplicationUtil';

@Injectable()
export class HttpClientCommon {

    private HTTPMETHOD = {
        GET: "GET",
        PUT: "PUT",
        POST: "POST",
        DELETE: "DELETE"
    }

    constructor(private http: HttpClient, private authService: AuthService) { }

    public firePostRequest(url, body) {

        let headers;
        if (this.authService.getIsAuthRequire(url)) {
            if (this.authService.getIsLoggedIn()) {
                headers = new HttpHeaders({
                    'Authorization': ApplicaionUtil.getAccessToken(),
                    'Content-type':  'application/json;charset=UTF-8',
                    'Accept':  'application/json;charset=UTF-8'
                })
                return this.http.post(url, body, {headers});
                
            } 
        } else {
            headers = new HttpHeaders({
                'Content-type':  'application/json;charset=UTF-8',
                'Accept':  'application/json;charset=UTF-8'
            });
            return this.http.post(url, body, {headers});
        }

    }

    public fireGetRequest(url) {

        let headers;
        if (this.authService.getIsAuthRequire(url)) {
            if (this.authService.getIsLoggedIn()) {
                headers = new HttpHeaders({
                    'Authorization': ApplicaionUtil.getAccessToken(),
                    'Accept':  'application/json;charset=UTF-8'
                })
                return this.http.get(url, {headers});
                
            } else {
                console.log("User session not valid");
                return null;
            }
        } else {
            headers = new HttpHeaders({
                'Accept':  'application/json;charset=UTF-8'
            });
            return this.http.get(url, {headers});
        }

    }

    public firePutRequest(url, body) {

        let headers;
        if (this.authService.getIsAuthRequire(url)) {
            if (this.authService.getIsLoggedIn()) {
                headers = new HttpHeaders({
                    'Authorization': ApplicaionUtil.getAccessToken(),
                    'Content-type':  'application/json;charset=UTF-8'
                })
                return this.http.put(url, body, {headers});
                
            }  
        } else {
            headers = new HttpHeaders({
                'Accept':  'application/json;charset=UTF-8'
            });
            return this.http.put(url, body, {headers});
        }

    }

    public fireDeleteRequest(url) {

        let headers;
        if (this.authService.getIsAuthRequire(url)) {
            if (this.authService.getIsLoggedIn()) {
                headers = new HttpHeaders({
                    'Authorization': ApplicaionUtil.getAccessToken(),
                })
                return this.http.delete(url, {headers});
                
            }  
        } else {
            
            return this.http.delete(url, {headers});
        }

    }
}
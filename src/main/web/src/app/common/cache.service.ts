import { Injectable } from "@angular/core";
import { Employee } from "../model/Employee";
import { Initiative } from "../model/Initiative";

export class CacheService {

    private projectId;
    private managerList;
    private managerEmployeeMap: Map<String, Array<Employee>>;
    private employeeInitiativeMap: Map<String, Array<Initiative>>;
    private initiativesEmployeeMapping;

    private employeesManagerMapping;

    getInitiativesEmployeeMapping () {
        return this.initiativesEmployeeMapping;
    }

    setInitiativesEmployeeMapping(initiativesEmployeeMapping) {
        this.initiativesEmployeeMapping = initiativesEmployeeMapping;
    }

    getProjectId() {
        return this.projectId;
    }

    setProjectId(projectId) {
        this.projectId = projectId;
    }

    getManagerList() {
        return this.managerList;
    }

    setManagerList(managerList) {
        this.managerList = managerList;
    }

    getEmployeesManagerMapping () {
        return this.employeesManagerMapping;
    }

    setEmployeesManagerMapping (employeesManagerMapping) {
        this.employeesManagerMapping = employeesManagerMapping;
    }   


}
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicaionUtil } from '../ApplicationUtil';

export class AuthService {

    getIsLoggedIn() {

        if (ApplicaionUtil.getAccessToken()) {
            return true;
        }

        return false;
    }

    getIsAuthRequire(url: string) {

        if (url.includes('/api/v1')) {
            return true;
        }
        return false;
    }

}
import { Injectable } from "@angular/core";

@Injectable()
export class SystemInfoService {

    static isChromeDev = false;

    constructor() { }

    static getAPIHost() {
        return this.isChromeDev ? 'http://localhost:6060' : `http://${window.location.host}`;
    }
}
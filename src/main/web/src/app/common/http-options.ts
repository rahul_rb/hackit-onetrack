import { HttpParams } from "@angular/common/http";

export interface HttpOptions {
    headers: Headers;
    queryParams: HttpParams
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { RouterModule } from '@angular/router'; 
import { MatCardModule } from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginServiceService } from './login-component/login-service.service';
import { HttpClientModule } from '@angular/common/http';
import { RedirectLogingComponent } from './redirect-loging/redirect-loging.component'; 
import {RedirectLogingService} from './redirect-loging/redirect-loging.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { EmployeeComponent } from './employee/employee.component'; 
import { MatTableModule } from '@angular/material/table';
import { EmployeeService } from './employee/employee-service';
import { MatSelectModule } from '@angular/material/select';
import { AddEmployeeDialogComponent } from './add-employee-dialog/add-employee-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UtilityService } from './common/util.service';
import { DashboardResourceService } from './dashboard/dashboard-resource.service';
import { DashboardService } from './dashboard/dashboard.service';
import { HttpClientCommon } from './common/httpclient-common.service';
import { AuthService } from './common/auth.service';
import { SystemInfoService } from './common/systeminfo.service';
import { CacheService } from './common/cache.service';
import { InitiativeService } from './initiative/initiative-service';
import { InitiativeComponent } from './initiative/initiative.component';
import { AddInitiativeDialogComponent } from './add-initiative-dialog/add-initiative-dialog.component';
import { AddAllocationDialogComponent } from './dashboard/add-allocation-dialog.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { EmployeeResourceService } from './employee/employee-resource.service';
import { MatNativeDateModule } from '@angular/material/core'; 
import { HeaderComponent } from './dashboard/header.component'; 
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    RedirectLogingComponent,
    DashboardComponent,
    EmployeeComponent,
    AddEmployeeDialogComponent,
    InitiativeComponent,
    AddInitiativeDialogComponent,
    HeaderComponent,
    AddAllocationDialogComponent
  ] ,
  imports: [
    BrowserModule,
     
    FormsModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSidenavModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    // MatMenuModule,
    // MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatIconModule, 
    MatListModule,
    MatToolbarModule,
    RouterModule.forRoot([
      { path: 'redirect/dashboard', component: RedirectLogingComponent } ,
      { path: '', component: LoginComponentComponent }  ,
      { path: 'employee', component: EmployeeComponent } ,
      { path: 'initiative', component: InitiativeComponent } ,
      { path: 'dashboard', component: HeaderComponent } 
    ]),
  ], 
  
  providers: [InitiativeService,LoginServiceService, RedirectLogingService, EmployeeService, UtilityService, DashboardResourceService, DashboardService, HttpClientCommon, AuthService, SystemInfoService, CacheService, EmployeeResourceService, MatDatepickerModule],
  
  bootstrap: [AppComponent],
  entryComponents: [AddEmployeeDialogComponent]
})
export class AppModule { }

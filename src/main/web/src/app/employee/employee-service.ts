import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApplicaionUtil } from '../ApplicationUtil';
import { EmployeeResourceService } from './employee-resource.service';
import { HttpClientCommon } from '../common/httpclient-common.service';
import { SystemInfoService } from '../common/systeminfo.service';

@Injectable()
export class EmployeeService {

  private url = "http://localhost:6060/api/v1";
  constructor(private http: HttpClient, private employeeResource: EmployeeResourceService, private httpService: HttpClientCommon) { }


  fetchEmployeeListBasedOnManagerAndProject(managerId) {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId= ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");
      let path = "/employees?filterType=project&queryString=projectId=" + projectId + ",managerId=" + managerId;

      let fullUrl = this.url + path;

      return this.http.get(fullUrl,{
        headers: new HttpHeaders({
          'Accept':  'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + accessToken
        })
      });
    }
  }

  fetchManagerListPerProject() {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId= ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");
      let path = "/employees?filterType=project&queryString=projectId=" + projectId + ",employeeRole=MANAGER&limit=500&offset=0";

      let fullUrl = this.url + path;
      return this.http.get(fullUrl,{
        headers: new HttpHeaders({
          'Accept':  'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + accessToken
        })
      })
    }
  }

  addEmployeeDetails(employeeContent) {
    let accessToken = ApplicaionUtil.getAccessToken();
    if (accessToken) {
      let projectId= ApplicaionUtil.getDetailsFromAccessTokenObject("projectId");

      let path = "/employee";
      let fullUrl = this.url + path;

      employeeContent["projectId"] = projectId;

      let body = JSON.stringify(employeeContent);

      return this.http.post(fullUrl,
        body,{
        headers: new HttpHeaders({
          'Content-Type':  'application/json;charset=UTF-8',
          'Accept':  'application/json;charset=UTF-8',
          'Authorization': 'Bearer ' + accessToken
        })
      });

    }
  }

  getAvailableEmployees (callback) {

    let url = this.employeeResource.frameGetAvailableEmployeesURL();
    this.httpService.fireGetRequest(url)
    .subscribe((res) => {
      callback(res);
    }, (err) => {
      callback(err);
    })
  }

  getEmployeeById(id) {

    let url = this.employeeResource.frameGetEmployeeById(id);

    return this.httpService.fireGetRequest(url)
    
  }

}

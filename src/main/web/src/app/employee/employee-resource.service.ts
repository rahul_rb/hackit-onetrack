import { Injectable } from "@angular/core";
import { SystemInfoService } from "../common/systeminfo.service";

export class EmployeeResourceService {

    serverUrl = "http://localhost:6060";

    frameGetAvailableEmployeesURL () {

        let url = `${SystemInfoService.getAPIHost()}/api/v1/allocation/employees/available?dateStamp=${new Date().getTime()}`;
        return url;
    }

    frameGetEmployeeById(id) {
        let url = `${SystemInfoService.getAPIHost()}/api/v1/employee/${id}`;
        return url;
    }
}
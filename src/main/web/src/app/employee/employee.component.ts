import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './employee-service';
import { MatDialog } from '@angular/material/dialog';
import { AddEmployeeDialogComponent } from '../add-employee-dialog/add-employee-dialog.component';


export class EmployeeRow {
  id: string;
  userId: string;
  fullName: string;
  employeeRole: string;
  managerId: string;

  public EmployeeRow(uId,fName,empRole,mId) {
     
    this.userId = uId;
    this.fullName = fName;
    this.employeeRole = empRole;
    this.managerId = mId;
  }
} 
 

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  displayedColumns: string[] = ['userId', 'fullName', 'employeeRole'];
  dataSource = [];
  selectedManger  ="";
  managerList = [];


  constructor(private employeeService: EmployeeService,public addEmployeeDialog: MatDialog) { }

  ngOnInit(): void {
    this.fetchManagersList();
  }


  onManagerSelect(event,selectedManager) {
    this.fetchAllEmployeesBasedOnManagerAndProject(selectedManager);
  }

  fetchManagersList() {
    this.employeeService.fetchManagerListPerProject()
      .subscribe((response) => {
        let employeeListObj = response["employees"];
        let resultList =  employeeListObj["results"];
        let fullEmployeeList :EmployeeRow[] = [];
        for (let i in resultList) {
          let element = resultList[i];
          var emp = new EmployeeRow();
          emp.id = element["id"];
          emp.fullName = element["fullName"]; 

          this.managerList.push(emp);
        }
      });
  }

  fetchAllEmployeesBasedOnManagerAndProject(managerId) {
    this.dataSource = [];
    this.employeeService.fetchEmployeeListBasedOnManagerAndProject(managerId)
      .subscribe((response) => {
        this.dataSource = [];
        let employeeListObj = response["employees"];
        let resultList =  employeeListObj["results"];
        let fullEmployeeList :EmployeeRow[] = [];
        for (let i in resultList) {
          let element = resultList[i];
          var emp = new EmployeeRow();
          emp.userId = element["employeeId"];
          emp.fullName = element["fullName"];
          emp.employeeRole = element["employeeRole"]
          emp.managerId = element["managerId"]

          fullEmployeeList.push(emp);
        }

        this.dataSource = fullEmployeeList;
      });
  }


  openAddEmployeeDialog() {
    let dialogRef = this.addEmployeeDialog.open(AddEmployeeDialogComponent, {
      disableClose: true,
      width: '800px',
      data: {
         managerList: this.managerList
      }
    }).afterClosed().subscribe(function (changedContent) {
      if (changedContent) {
        this.fetchManagersList();
        if (this.selectedManger!="") {
          this.fetchAllEmployeesBasedOnManagerAndProject(this.selectedManger);
        }
        
      }
    }.bind(this));
  }
}

# One Track Ver. 0.0.1 #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###

* Cloud based solution to manage different tasks/initiatives, employees and allocations of the task and their timelines. 

### Tech Stack ###

* Postgres for Database 
* Spring Boot for backend service and REST API
* Angular Material for UI
* AWS (S3, EC2 Instance) for Deploying the project
* Bitbucket and AWS Code Deploy as CI/CD pipeline.
* CI Identity Provider as Auththentication

### Pre-requisites ###

* Postgres
* Java

### How do I get set up locally? ###

* Clone the project -> git clone https://bitbucket.org/rahul_rb/hackit-onetrack.git
* Open src/main/resources/application.properties
* Change the Database Username, Password accroding to your local setup.
* To Build run -> mvn clean install
* To Run Test -> mvn clean test
* To Run project in local -> java -jar target/one-track.0.0.1.jar 

 
### Who do I talk to? ###

* Contact Rahul and Subhajit
* Create Git Issue to create Bugs
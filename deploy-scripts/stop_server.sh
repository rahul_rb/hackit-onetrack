#!/bin/sh
FILE=/tmp/pid.file

if [[ -f "$FILE" ]]; then
    pid=$(<$FILE)
    kill -9 $pid
fi

